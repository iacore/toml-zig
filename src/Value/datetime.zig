const std = @import("std");
const testing = std.testing;
const ascii = std.ascii;
const fmt = std.fmt;

pub const DateTimeError = error{
    NotADateTime,
    BadFormat,
    NonRealsticDateTime,
};

pub const DateTime = struct {
    year: u16,
    month: u8,
    day: u8,
    hour: u8,
    minute: u8,
    second: Seconds,
    // Offset Fields
    // Sign is true when positive
    offSign: ?bool,
    offHour: ?u8,
    offMinute: ?u8,

    pub fn setString(input: []const u8) !DateTime {
        if (!try isDateTime(input)) {
            return DateTimeError.NotADateTime;
        }
        // initialize it to the end of the string
        var zindex: usize = input.len - 1;
        // find index of Z, z, +, or - from the end.
        var i = input.len - 1;
        while (i > 17) : (i -= 1) {
            var c = input[i];
            if (c == 'z' or c == 'Z' or c == '-' or c == '+') {
                // correct the index for the seconds
                zindex = i;
                break;
            }
        }
        if (zindex == input.len - 1) {
            return DateTime{
                .year = try fmt.parseUnsigned(u16, input[0..4], 10),
                .month = try fmt.parseUnsigned(u8, input[5..7], 10),
                .day = try fmt.parseUnsigned(u8, input[8..10], 10),
                .hour = try fmt.parseUnsigned(u8, input[11..13], 10),
                .minute = try fmt.parseUnsigned(u8, input[14..16], 10),
                .second = switch (input.len) {
                    0...20 => Seconds{ .decimal = try fmt.parseUnsigned(u8, input[17..zindex], 10) },
                    else => Seconds{ .fraction = try fmt.parseFloat(f32, input[17..zindex]) },
                },
                .offHour = null,
                .offSign = null,
                .offMinute = null,
            };
        } else {
            return DateTime{
                .year = try fmt.parseUnsigned(u16, input[0..4], 10),
                .month = try fmt.parseUnsigned(u8, input[5..7], 10),
                .day = try fmt.parseUnsigned(u8, input[8..10], 10),
                .hour = try fmt.parseUnsigned(u8, input[11..13], 10),
                .minute = try fmt.parseUnsigned(u8, input[14..16], 10),
                .second = switch (input.len) {
                    0...20 => Seconds{ .decimal = try fmt.parseUnsigned(u8, input[17..zindex], 10) },
                    else => Seconds{ .fraction = try fmt.parseFloat(f32, input[17..zindex]) },
                },
                .offHour = try fmt.parseUnsigned(u8, input[zindex + 1 .. zindex + 3], 10),
                .offSign = '+' == input[zindex],
                .offMinute = try fmt.parseUnsigned(u8, input[zindex + 4 ..], 10),
            };
        }
    }

    //http://howardhinnant.github.io/date_algorithms.html#civil_from_days
    pub fn fromTimestamp(seconds: u64) DateTime {
        //extract hours: https://stackoverflow.com/questions/67943550/how-to-extract-the-hour-of-day-from-a-timestamp-by-hand
        var hr: u8 = @as(u8, @intCast((seconds % 86400) / 3600));
        var min: u8 = @as(u8, @intCast(((seconds % 86400) % 3600) / 60));
        var sec: u8 = @as(u8, @intCast(((seconds % 86400) % 3600) % 60));
        var days = seconds / 86400;
        var z = days + 719468;
        var era: u64 = undefined;
        if (z >= 0) {
            era = z / 146097;
        } else {
            era = (z - 146096) / 146097;
        }
        var doe: u64 = z - era * 146097;
        var yoe: u64 = (doe - doe / 1460 + doe / 36524 - doe / 146096) / 365;
        var y = @as(u16, @intCast(yoe + era * 400));
        var doy = doe - (365 * yoe + yoe / 4 - yoe / 100);
        var mp = (5 * doy + 2) / 153;
        var d = @as(u8, @intCast(doy - (153 * mp + 2) / 5 + 1));
        var m: u8 = undefined;
        if (mp < 10) {
            m = @as(u8, @intCast(mp + 3));
        } else {
            m = @as(u8, @intCast(mp - 9));
        }
        if (m <= 2) {
            y += 1;
        }
        return .{
            .year = y,
            .month = m,
            .day = d,
            .hour = hr,
            .minute = min,
            .second = .{ .decimal = sec },
            .offSign = null,
            .offHour = null,
            .offMinute = null,
        };
    }

    pub fn fromMilliTimestamp(seconds: u64) DateTime {
        //extract hours: https://stackoverflow.com/questions/67943550/how-to-extract-the-hour-of-day-from-a-timestamp-by-hand

        var float_seconds = @as(f128, @floatFromInt(seconds));
        float_seconds /= 1_000_000;
        //var ns_only = @mod(float_seconds, 1);
        //var f_to_int = @floatToInt(u64, @floor(float_seconds));
        var days = seconds / (86400 * 1_000_000);
        //var days_float = float_seconds / (86400 * 1_000_000_000);

        var sec: f128 = @rem(@rem(@rem(float_seconds, 86400), 3600), 60);
        //var sec: f32 = @rem(float_seconds, 86400);
        var hr: u8 = @as(u8, @intCast(((seconds / 1_000_000) % 86400) / 3600));
        var min: u8 = @as(u8, @intCast((((seconds / 1_000_000) % 86400) % 3600) / 60));
        var z = days + 719468;
        var era: u64 = undefined;
        if (z >= 0) {
            era = z / 146097;
        } else {
            era = (z - 146096) / 146097;
        }
        var doe: u128 = z - era * 146097;
        var yoe: u128 = (doe - doe / 1460 + doe / 36524 - doe / 146096) / 365;
        var y = @as(u16, @intCast(yoe + era * 400));
        var doy = doe - (365 * yoe + yoe / 4 - yoe / 100);
        var mp = (5 * doy + 2) / 153;
        var d = @as(u8, @intCast(doy - (153 * mp + 2) / 5 + 1));
        var m: u8 = undefined;
        if (mp < 10) {
            m = @as(u8, @intCast(mp + 3));
        } else {
            m = @as(u8, @intCast(mp - 9));
        }
        if (m <= 2) {
            y += 1;
        }
        return .{
            .year = y,
            .month = m,
            .day = d,
            .hour = hr,
            .minute = min,
            .second = .{ .fraction = sec },
            .offSign = null,
            .offHour = null,
            .offMinute = null,
        };
    }

    pub fn fromNanoTimestamp(seconds: u128) DateTime {
        //extract hours: https://stackoverflow.com/questions/67943550/how-to-extract-the-hour-of-day-from-a-timestamp-by-hand

        var float_seconds = @as(f128, @floatFromInt(seconds));
        float_seconds /= 1_000_000_000;
        //var ns_only = @mod(float_seconds, 1);
        //var f_to_int = @floatToInt(u64, @floor(float_seconds));
        var days = seconds / (86400 * 1_000_000_000);
        //var days_float = float_seconds / (86400 * 1_000_000_000);

        var sec: f128 = @rem(@rem(@rem(float_seconds, 86400), 3600), 60);
        //var sec: f32 = @rem(float_seconds, 86400);
        var hr: u8 = @as(u8, @intCast(((seconds / 1_000_000_000) % 86400) / 3600));
        var min: u8 = @as(u8, @intCast((((seconds / 1_000_000_000) % 86400) % 3600) / 60));
        var z = days + 719468;
        var era: u128 = undefined;
        if (z >= 0) {
            era = z / 146097;
        } else {
            era = (z - 146096) / 146097;
        }
        var doe: u128 = z - era * 146097;
        var yoe: u128 = (doe - doe / 1460 + doe / 36524 - doe / 146096) / 365;
        var y = @as(u16, @intCast(yoe + era * 400));
        var doy = doe - (365 * yoe + yoe / 4 - yoe / 100);
        var mp = (5 * doy + 2) / 153;
        var d = @as(u8, @intCast(doy - (153 * mp + 2) / 5 + 1));
        var m: u8 = undefined;
        if (mp < 10) {
            m = @as(u8, @intCast(mp + 3));
        } else {
            m = @as(u8, @intCast(mp - 9));
        }
        if (m <= 2) {
            y += 1;
        }
        return .{
            .year = y,
            .month = m,
            .day = d,
            .hour = hr,
            .minute = min,
            .second = .{ .fraction = sec },
            .offSign = null,
            .offHour = null,
            .offMinute = null,
        };
    }

    pub fn toNanoTimestamp(self: *DateTime) i128 {
        var ret = switch (self.second) {
            Seconds.fraction => |f| @as(i128, @intFromFloat(f * 1000000000)),
            Seconds.decimal => |d| @as(i128, d) * 1000000000,
        };
        var year = @as(i128, self.year);
        if (self.month <= 2) {
            year -= 1;
        }
        if (year < 0) {
            year -= 399;
        }
        var era = @divFloor(year, 400);
        var yoe = (year - era * 400);
        var doy = @as(i128, self.month);
        if (doy > 2) {
            doy -= 3;
        } else {
            doy += 9;
        }
        doy = @divFloor((153 * doy + 2), 5) + self.day - 1;
        var doe = yoe * 365 + @divFloor(yoe, 4) - @divFloor(yoe, 100) + doy;
        doe = era * 146097 + doe - 719468;

        ret += (@as(i128, self.minute) * 60 * 1000000000);
        ret += (@as(i128, self.hour) * 60 * 60 * 1000000000);
        if (self.offSign orelse false) {
            ret -= (@as(i128, self.offHour orelse 0) * 60 * 60 * 1000000000);
            ret -= (@as(i128, self.offMinute orelse 0) * 60 * 60 * 1000000000);
        } else {
            ret += (@as(i128, self.offHour orelse 0) * 60 * 60 * 1000000000);
            ret += (@as(i128, self.offMinute orelse 0) * 60 * 60 * 1000000000);
        }
        return ret + (doe * 24 * 60 * 60 * 1000000000);
    }
    pub fn toMilliTimestamp(self: *DateTime) i64 {
        var ret = switch (self.second) {
            Seconds.fraction => |f| @as(i64, @intFromFloat(f * 1000)),
            Seconds.decimal => |d| @as(i64, d) * 1000,
        };
        var year = @as(i64, self.year);
        if (self.month <= 2) {
            year -= 1;
        }
        if (year < 0) {
            year -= 399;
        }
        var era = @divFloor(year, 400);
        var yoe = (year - era * 400);
        var doy = @as(i64, self.month);
        if (doy > 2) {
            doy -= 3;
        } else {
            doy += 9;
        }
        doy = @divFloor((153 * doy + 2), 5) + self.day - 1;
        var doe = yoe * 365 + @divFloor(yoe, 4) - @divFloor(yoe, 100) + doy;
        doe = era * 146097 + doe - 719468;

        ret += (@as(i64, self.minute) * 60 * 1000);
        ret += (@as(i64, self.hour) * 60 * 60 * 1000);
        if (self.offSign orelse false) {
            ret -= (@as(i64, self.offHour orelse 0) * 60 * 60 * 1000);
            ret -= (@as(i64, self.offMinute orelse 0) * 60 * 60 * 1000);
        } else {
            ret += (@as(i64, self.offHour orelse 0) * 60 * 60 * 1000);
            ret += (@as(i64, self.offMinute orelse 0) * 60 * 60 * 1000);
        }
        return ret + (doe * 24 * 60 * 60 * 1000);
    }
    //http://howardhinnant.github.io/date_algorithms.html#days_from_civil
    pub fn toTimestamp(self: *DateTime) i64 {
        var ret = switch (self.second) {
            Seconds.fraction => |f| @as(i64, @intFromFloat(f)),
            Seconds.decimal => |d| @as(i64, d),
        };
        var year = @as(i64, self.year);
        if (self.month <= 2) {
            year -= 1;
        }
        if (year < 0) {
            year -= 399;
        }
        var era = @divFloor(year, 400);
        var yoe = (year - era * 400);
        var doy = @as(i64, self.month);
        if (doy > 2) {
            doy -= 3;
        } else {
            doy += 9;
        }
        doy = @divFloor((153 * doy + 2), 5) + self.day - 1;
        var doe = yoe * 365 + @divFloor(yoe, 4) - @divFloor(yoe, 100) + doy;
        doe = era * 146097 + doe - 719468;
        ret += (@as(i64, self.minute) * 60);
        ret += (@as(i64, self.hour) * 60 * 60);
        if (self.offSign orelse false) {
            ret -= (@as(i64, self.offHour orelse 0) * 60 * 60 * 1000);
            ret -= (@as(i64, self.offMinute orelse 0) * 60 * 60 * 1000);
        } else {
            ret += (@as(i64, self.offHour orelse 0) * 60 * 60 * 1000);
            ret += (@as(i64, self.offMinute orelse 0) * 60 * 60 * 1000);
        }
        return ret + (doe * 24 * 60 * 60);
    }
};

const Seconds = union(enum) {
    fraction: f128,
    decimal: u8,
};

pub fn isDateOrTime(input: []const u8) !bool {
    if (input.len >= 19) {
        return try isDateTime(input);
    }
    // Check if The last char is a number. In this case we already know it's going to be either
    // time or a date and NOT DateTime.
    if (!ascii.isDigit(input[input.len - 1])) {
        return false;
    }
    return try isDate(input) or try isTime(input);
}

fn isDateTime(input: []const u8) !bool {
    if (input.len < 19) {
        return false;
    }
    if (input[10] != 'T' and input[10] != 't' and input[10] != ' ') {
        return false;
    }
    return try isTime(input[11..]) and try isDate(input[0..10]);
}
fn isTime(input: []const u8) !bool {
    if (input.len < 8) {
        return false;
    }
    if (input[2] != ':' or input[5] != ':') {
        return false;
    }
    var hour: u8 = std.fmt.parseInt(u8, input[0..2], 10) catch return false;
    if (hour < 0 or hour > 23) {
        return DateTimeError.NonRealsticDateTime;
    }
    var minute: u8 = std.fmt.parseInt(u8, input[3..5], 10) catch return false;
    if (minute < 0 or minute > 59) {
        return DateTimeError.NonRealsticDateTime;
    }
    var seconds: u8 = std.fmt.parseInt(u8, input[6..8], 10) catch return false;
    if (seconds < 0 or seconds > 60) {
        return DateTimeError.NonRealsticDateTime;
    }
    if (input.len == 8) {
        return true;
    }
    var nums: usize = 0;
    if (input[8] == '.') {
        for (input[9..]) |char| {
            nums += 1;
            if (!ascii.isDigit(char)) {
                break;
            }
        }
        if (nums == 0) {
            return false;
        }
    }
    if (input.len == nums + 9) {
        return true;
    }

    if ((input[nums + 8] == 'Z' or input[nums + 8] == 'z') and input.len == nums + 9) {
        return true;
    }
    if (input[nums + 8] != '+' and input[nums + 8] != '-') {
        return false;
    }
    var offHour: u8 = std.fmt.parseInt(u8, input[nums + 9 .. nums + 11], 10) catch return false;
    if (offHour < 0 or offHour > 23) {
        return DateTimeError.NonRealsticDateTime;
    }
    var offMinute: u8 = std.fmt.parseInt(u8, input[nums + 12 .. nums + 14], 10) catch return false;
    if (offMinute < 0 or offMinute > 59) {
        return DateTimeError.NonRealsticDateTime;
    }
    if (input.len != nums + 14) {
        return false;
    }
    return true;
}

fn isDate(input: []const u8) !bool {
    if (input.len != 10) {
        return false;
    }
    if (input[4] != '-' and input[7] != '-') {
        return false;
    }
    var year = std.fmt.parseInt(u16, input[0..4], 10) catch return false;
    var month = std.fmt.parseInt(u8, input[5..7], 10) catch return false;
    var day = std.fmt.parseInt(u8, input[8..10], 10) catch return false;
    if (day == 0) {
        return false;
    }
    var leapYear = (year % 4 == 0 and year % 100 != 0) or (year % 400 == 0);
    switch (month) {
        1, 3, 5, 7, 8, 10, 12 => {
            if (day > 31) {
                return DateTimeError.NonRealsticDateTime;
            }
        },
        4, 6, 9, 11 => {
            if (day > 30) {
                return DateTimeError.NonRealsticDateTime;
            }
        },
        2 => {
            if (leapYear) {
                if (day > 29) {
                    return DateTimeError.NonRealsticDateTime;
                }
            } else {
                if (day > 28) {
                    return DateTimeError.NonRealsticDateTime;
                }
            }
        },
        else => return DateTimeError.NonRealsticDateTime,
    }

    return true;
}

test "Garbage Input" {
    try testing.expectEqual(false, try isDateTime("A123-AB-CDT07:32:00"));
}

test "Test Not Leap" {
    try testing.expectError(DateTimeError.NonRealsticDateTime, isDateTime("1977-02-29T07:32:00"));
}
test "Test Leap" {
    try testing.expectEqual(true, try isDateTime("1976-02-29T07:32:00"));
}
test "Should Fail" {
    try testing.expectEqual(false, try isDateTime("1979-05-27T07:32:00."));
}
test "Should Fail1" {
    try testing.expectEqual(false, try isDateTime("1979-05-27T07:32:00Z-11:30"));
}
test "Should Fail2" {
    try testing.expectEqual(false, try isDateTime("1979-05-27T07:32:00Z-11:301"));
}

test "Validate" {
    try testing.expectEqual(true, try isDateTime("1979-05-27T07:32:00Z"));
}
test "Validate1" {
    try testing.expectEqual(true, try isDateTime("1979-05-27T00:32:00-07:00"));
}
test "Validate2" {
    try testing.expectEqual(true, try isDateTime("1979-05-27T00:32:00.999999-07:00"));
}
test "Validate3" {
    try testing.expectEqual(true, try isDateTime("1979-05-27 07:32:00Z"));
}

test "Validate4" {
    try testing.expectEqual(true, try isDateTime("1979-05-27T07:32:00"));
}

test "Validate5" {
    try testing.expectEqual(true, try isDateTime("1979-05-27T00:32:00.999999"));
}
test "Validate6" {
    try testing.expectEqual(true, try isDateTime("1979-05-27T00:32:00.999999Z"));
}
test "Vali(Date)" {
    try testing.expectEqual(true, try isDate("1979-05-27"));
}

test "Validate Time" {
    try testing.expectEqual(true, try isTime("00:32:00.999999"));
}

test "Not Time" {
    try testing.expectEqual(false, try isDateOrTime("00:32:00.999999Z"));
}
test "Not Date" {
    try testing.expectEqual(false, try isDate("1979-05-27T"));
}
test "0Date" {
    try testing.expectEqual(false, try isDate("1979-01-00"));
}

test "RFC 3339 Docs" {
    try testing.expectEqual(true, try isDateOrTime("1937-01-01T12:00:27.87+00:20"));
}

test "DateTime setString" {
    _ = try DateTime.setString("1979-05-27T07:32:00");
    //std.debug.print("{any}, Seconds: {}\n", .{ dt, dt.second.decimal });
}
test "DateTime setString1" {
    _ = try DateTime.setString("1979-05-27T07:32:00.23+00:30");
    //std.debug.print("{any}, Seconds: {}\n", .{ dt, dt.second.fraction });
}
test "DateTime setString2" {
    var dt = try DateTime.setString("1979-05-27T07:32:00Z");
    try testing.expectEqual(dt.year, 1979);
    try testing.expectEqual(dt.month, 5);
    try testing.expectEqual(dt.day, 27);
    try testing.expectEqual(dt.hour, 7);
    try testing.expectEqual(dt.minute, 32);
    try testing.expectEqual(dt.second.decimal, 0);
}

test "DateTime Garbage" {
    try testing.expectError(DateTimeError.NotADateTime, DateTime.setString("ABCD-EF-GHT"));
}

test "toTimestamp" {
    var dt = try DateTime.setString("1978-05-27T07:32:10Z");
    try testing.expectEqual(@as(i64, 265102330), dt.toTimestamp());
}

test "toTimestamp2" {
    var dt = try DateTime.setString("1970-05-27T07:32:10Z");
    try testing.expectEqual(@as(i64, 12641530), dt.toTimestamp());
}
test "toMilliTimestamp" {
    var dt = try DateTime.setString("1976-05-27T07:32:10.97535Z");
    try testing.expectEqual(@as(i64, 202030330975), dt.toMilliTimestamp());
}
test "toNanoTimestamp" {
    var dt = try DateTime.setString("1976-05-27T07:32:10.97535Z");
    try testing.expectEqual(@as(i128, 202030330975350379), dt.toNanoTimestamp());
}

test "Offset Timestamp" {
    var dt = try DateTime.setString("2022-08-06T20:26:07.888-05:00");
    try testing.expectEqual(@as(i64, 1659835567888), dt.toMilliTimestamp());
}

test "unix to datetime" {
    var time: u64 = 1665636651;
    var dt = DateTime.fromTimestamp(time);
    try testing.expectEqual(@as(u8, 4), dt.hour);
}

test "nano" {
    var time: u128 = 1665672541722311000;

    var dt = DateTime.fromNanoTimestamp(time);
    try testing.expectEqual(@as(u8, 14), dt.hour);
    try testing.expectEqual(@as(u8, 49), dt.minute);
    // BUG: Zig's formatter doesn't like f128 for whatever reason.
    // + expectEqual doesn't like the output even though they are the same'ish
    //try testing.expectEqual(@as(f128, 1.722311), dt.second.fraction);
}
test "milli" {
    var time: u64 = 1665672541722311;

    var dt = DateTime.fromMilliTimestamp(time);
    try testing.expectEqual(@as(u8, 14), dt.hour);
    try testing.expectEqual(@as(u8, 49), dt.minute);
    // BUG: Zig's formatter doesn't like f128 for whatever reason.
    // + expectEqual doesn't like the output even though they are the same'ish
    //try testing.expectEqual(@as(f128, 1.722311), dt.second.fraction);
}
