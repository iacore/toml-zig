const std = @import("std");
const testing = std.testing;
const fmt = std.fmt;
const test_allocator = std.testing.allocator;
const Allocator = std.mem.Allocator;
const ArrayList = std.ArrayList;

const StringError = error{ InvalidNumberOfDigits, BadEscape, InvalidScalarValue, UnEscapedQuote, MultilineStringInSingleQuote, BadNumberOfSingleQuotes, NewLineInNonMultilineString };

pub fn isString(input: []const u8) StringError!bool {
    if (input.len < 2) {
        return false;
    }
    if (input[input.len - 1] == '\'' and input[0] == '\'') {
        return isLiteral(input);
    }
    if (input[input.len - 1] != '"' and input[0] != '"') {
        return false;
    }
    var multiline = false;
    var start: usize = 1;
    var end: usize = input.len - 1;
    if (input[input.len - 2] == '"' and input[input.len - 3] == '"' and input[1] == '"' and input[2] == '"' and input.len > 3) {
        start = 3;
        end = input.len - 3;
        multiline = true;
    }
    var skip = false;
    if (!multiline and std.mem.containsAtLeast(u8, input[start..end], 1, "\n")) {
        return StringError.NewLineInNonMultilineString;
    }
    var iterToken = std.mem.split(u8, input[start..end], "\n");
    while (iterToken.next()) |l| {
        var line = l;
        if (multiline) {
            line = std.mem.trimRight(u8, line, " ");
        }
        var qcount: u8 = 0;
        for (line, 0..) |char, idx| {
            if (skip) {
                skip = false;
                continue;
            }
            switch (char) {
                '\\' => {
                    if (idx == line.len - 1 and multiline) {
                        continue;
                    }
                    // We have reached the end of the string but the escape is escaping the "
                    if (idx == line.len - 1) {
                        std.log.err("Bad Escape {s}\n", .{input});
                        return StringError.BadEscape;
                    }
                    var next = line[idx + 1];
                    if (next == '\\') {
                        skip = true;
                        continue;
                    }
                    if (next == '"') {
                        qcount = 0;
                        continue;
                    }
                    if (next == 'b' or next == 't' or next == 'n' or next == 'f' or next == 'r') {
                        continue;
                    } else if (next == 'u') {
                        if (line.len - idx < 4) {
                            return StringError.InvalidNumberOfDigits;
                        }
                        var scalar = fmt.parseUnsigned(u16, line[idx + 2 .. idx + 6], 16) catch return StringError.InvalidScalarValue;
                        if (scalar > 0xD7FF and !(scalar >= 0xE000 and scalar <= 0xFFFF)) {
                            return StringError.InvalidScalarValue;
                        }
                    } else if (next == 'U') {
                        if (input.len - idx < 8) {
                            return StringError.InvalidNumberOfDigits;
                        }
                        var scalar = fmt.parseUnsigned(u32, line[idx + 2 .. idx + 10], 16) catch return StringError.InvalidScalarValue;
                        if (scalar > 0xD7FF and !(scalar >= 0xE000 and scalar <= 0x10FFFF)) {
                            return StringError.InvalidScalarValue;
                        }
                    } else {
                        return StringError.BadEscape;
                    }
                },
                '"' => {
                    if (input[idx] != '\\') {
                        if (multiline and qcount < 2) {
                            qcount += 1;
                        } else {
                            return StringError.UnEscapedQuote;
                        }
                    }
                },
                else => {
                    qcount = 0;
                    continue;
                },
            }
        }
    }
    return true;
}

// This is a branch of the isString function to parse single quoted strings, AKA: literals.
// Since string literals have no escape whatsoever, we don't care about anything.
// The exception is having a single quote in the middle of the string.
// TODO: test Zig's builtin string search to find any '\'' or "'''" compared to this manual method.
fn isLiteral(input: []const u8) StringError!bool {
    // Because we already know from isString that the input is >= 2 and if we got here,
    // it means that we have single quote in both ends.
    //
    //if (input.len < 2) {
    //    return false;
    //}
    // if (input[input.len - 1] != '\'' and input[0] != '\'') {
    //     return false;
    // }
    var multiline = false;
    var start: usize = 1;
    var end: usize = input.len - 1;
    if (input[input.len - 2] == '\'' and input[input.len - 3] == '\'' and input[1] == '\'' and input[2] == '\'' and input.len > 3) {
        start = 3;
        end = input.len - 3;
        multiline = true;
    }
    var iterToken = std.mem.split(u8, input[start..end], "\n");
    while (iterToken.next()) |*line| {
        var qcount: u8 = 0;
        for (line.*, 0..) |char, idx| {
            switch (char) {
                '\'' => {
                    if (input[idx] == '\'') {
                        if (multiline and qcount < 2) {
                            qcount += 1;
                        } else {
                            return StringError.BadNumberOfSingleQuotes;
                        }
                    }
                },
                else => {
                    qcount = 0;
                    continue;
                },
            }
        }
    }
    return true;
}

fn fromLiteralToString(input: []const u8) []const u8 {
    if (input[input.len - 2] == '\'' and input[input.len - 3] == '\'' and input[1] == '\'' and input[2] == '\'' and input.len > 3) {
        return input[3 .. input.len - 3];
    }
    return input[1 .. input.len - 1];
}

pub fn toString(alloc: Allocator, input: []const u8) ![]u8 {
    var ecounter: usize = 0;
    var start: usize = 1;
    var end: usize = input.len - 1;
    var ret: []u8 = undefined;
    if (input[input.len - 2] == '"' and input[input.len - 3] == '"' and input[1] == '"' and input[2] == '"' and input.len > 3) {
        start = 3;
        end = input.len - 3;
        ret = try alloc.alloc(u8, input.len - 6);
    } else {
        ret = try alloc.alloc(u8, input.len - 2);
        std.mem.copy(u8, ret, input[start..end]);
    }
    if (start == 3) {
        var iterToken = std.mem.split(u8, input[start..end], "\n");
        var idx: usize = 0;
        while (iterToken.next()) |l| {
            var line = l;
            var prevlen = line.len;
            var trimmed = std.mem.trimRight(u8, line, " ");
            if (trimmed.len > 0 and trimmed[trimmed.len - 1] == '\\') {
                line = trimmed[0 .. trimmed.len - 1];
                ecounter += prevlen - line.len;
                std.mem.copy(u8, ret[idx..], trimmed[0 .. trimmed.len - 1]);
                std.mem.copy(u8, ret[idx + trimmed.len - 1 ..], "\n");
                idx += trimmed.len;
                continue;
            }
            std.mem.copy(u8, ret[idx..], line);
            idx += line.len;
            if (ret.len > idx) {
                std.mem.copy(u8, ret[idx..], "\n");
                idx += 1;
            }
        }
    }
    ecounter += std.mem.replace(u8, ret, "\\n", "\n", ret);
    ecounter += std.mem.replace(u8, ret, "\\b", "\u{0008}", ret);
    ecounter += std.mem.replace(u8, ret, "\\t", "\t", ret);
    ecounter += std.mem.replace(u8, ret, "\\f", "\u{000C}", ret);
    ecounter += std.mem.replace(u8, ret, "\\r", "\r", ret);
    ecounter += std.mem.replace(u8, ret, "\\\"", "\"", ret);
    ecounter += std.mem.replace(u8, ret, "\\\\", "\\", ret);

    var tokenizedu = std.mem.split(u8, input[start..end], "\\u");
    _ = tokenizedu.next();
    var replaced: [8]u8 = undefined;
    var replen: usize = 0;
    while (tokenizedu.next()) |next| {
        if (std.mem.eql(u8, replaced[0..4], next[0..4])) {
            ecounter += replen;
            continue;
        }
        @memcpy(replaced[0..4], next[0..4]);
        var buffer: [10]u8 = undefined;
        var parsed = try std.fmt.parseUnsigned(u21, next[0..4], 16);
        var encoded = try std.unicode.utf8Encode(parsed, &buffer);
        var buffer2: [6]u8 = undefined;
        var found = try std.fmt.bufPrint(&buffer2, "\\u{s}", .{next[0..4]});
        _ = (std.mem.replace(u8, ret, found, buffer[0..encoded], ret));
        replen = found.len - encoded;
        ecounter += replen;
    }
    var tokenizedU = std.mem.split(u8, input[start..end], "\\U");
    _ = tokenizedU.next();
    while (tokenizedU.next()) |next| {
        if (std.mem.eql(u8, &replaced, next[0..8])) {
            ecounter += replen;
            continue;
        }
        @memcpy(&replaced, next[0..8]);
        var buffer: [10]u8 = undefined;
        var parsed = try std.fmt.parseUnsigned(u21, next[0..8], 16);
        var encoded = try std.unicode.utf8Encode(parsed, &buffer);
        var buffer2: [10]u8 = undefined;
        var found = try std.fmt.bufPrint(&buffer2, "\\U{s}", .{next[0..8]});
        _ = (std.mem.replace(u8, ret, found, buffer[0..encoded], ret));
        replen = found.len - encoded;
        ecounter += replen;
    }

    return ret[0 .. ret.len - ecounter];
}

test "Spec Test 1" {
    var tf =
        \\"I'm a string. \"You can quote me\". Name\tJos\nLocation\tSF.\uD7FF"
    ;
    try testing.expect(try isString(tf));
}

test "Spec Test 2" {
    var tf =
        \\"I'm a string. \"You can quote me\". Name\tJos\U0010FFFF\nLocation\tSF."
    ;
    try testing.expect(try isString(tf));
}

test "Bad Escape 1" {
    var tf =
        \\"I'm a string. \"You can quote me\. Name\tJos\U0010FFFF\nLocation\tSF."
    ;
    try testing.expectError(StringError.BadEscape, isString(tf));
}
test "Bad Escape 2" {
    var tf =
        \\"I'm a string. "You can quote me. Name\tJos\U0010FFFF\nLocation\tSF."
    ;
    try testing.expectError(StringError.UnEscapedQuote, isString(tf));
}
test "Good Escape" {
    var tf =
        \\"I'm a string. \\You"
    ;
    try testing.expect(try isString(tf));
}

test "Multiline String 1" {
    var tf =
        \\"""I'm a multiline string. \\You\
        \\Can't see mewq
        \\"""
    ;
    try testing.expect(try isString(tf));
}
test "Multiline String 2" {
    var tf =
        \\"""I'm a multiline string. \\You \            
        \\Can't see me
        \\"""
    ;
    try testing.expect(try isString(tf));
}
test "Multiline String 3" {
    var tf =
        \\"""I'm a multiline string. \\You\\ \
        \\Can't see me
        \\"""
    ;
    try testing.expect(try isString(tf));
}
test "Multiline String 4" {
    var tf =
        \\"""I'm a multiline string. \\You\\ \
        \\Can't see me ""
        \\"""
    ;
    try testing.expect(try isString(tf));
}

test "Multiline String 5" {
    var tf =
        \\"""Here are fifteen quotation marks: ""\"""\"""\"""\"""\"."""
    ;
    try testing.expect(try isString(tf));
}

test "Single Quote" {
    var tf =
        \\'\\ServerX\admin$\system32\'
    ;
    try testing.expect(try isLiteral(tf));
}

test "Multiline Literal" {
    var tf =
        \\'''
        \\The first newline is
        \\trimmed in raw strings.
        \\All other whitespace
        \\is preserved.
        \\\'''
    ;
    try testing.expect(try isLiteral(tf));
    try testing.expect(try isString(tf));
}

test "Also a Valid Multiline Literal" {
    var tf =
        \\'''
        \\The first newline is
        \\trimmed in raw strings.''
        \\'All other whitespace
        \\is preserved.
        \\\'''
    ;
    try testing.expect(try isLiteral(tf));
    try testing.expect(try isString(tf));
}

test "Multiline Literal But Single Lined" {
    var tf =
        \\'''I [dw]on't need \d{2} apples'''
    ;
    try testing.expect(try isLiteral(tf));
}

test "Multiline String 6" {
    var tf =
        \\"""
    ;
    try testing.expectError(StringError.UnEscapedQuote, isString(tf));
}

test "Multiline Literal String" {
    var tf =
        \\''''''
    ;
    try testing.expect(try isString(tf));
}

test "fromLiteralMultilineToString" {
    var tf =
        \\'''HELLO 
        \\World'''
    ;
    var parsed = fromLiteralToString(tf);
    try testing.expect(std.mem.eql(u8, "HELLO \nWorld", parsed));
}

test "fromLiteralToString" {
    var tf =
        \\'Hello World'
    ;
    var parsed = fromLiteralToString(tf);
    try testing.expect(std.mem.eql(u8, "Hello World", parsed));
}

test "toString newline" {
    var tf =
        \\"""\tHel\\lo \u24D0  \"Worl\bd\n\f"""
    ;
    var parsed = try toString(test_allocator, tf);
    defer test_allocator.free(parsed);
    try testing.expect(std.mem.eql(u8, "\tHel\\lo ⓐ  \"Worl\u{008}d\n\u{00C}", parsed));
}
test "toString unicode" {
    var tf =
        \\"""\u24D0\n\u0302\u24D0\u24D0\u0302\u0302\u0302\u0302\u0302\u0302"""
    ;
    var parsed = try toString(test_allocator, tf);
    defer test_allocator.free(parsed);
    try testing.expect(std.mem.eql(u8, "ⓐ\n̂ⓐⓐ̂̂̂̂̂̂", parsed));
}

test "Multiline String 7" {
    var tf =
        \\"""\
        \\I'm a multiline string. \\You \            
        \\Can't see
        \\
        \\me"""
    ;
    var parsed = try toString(test_allocator, tf);
    defer test_allocator.free(parsed);
    try testing.expect(std.mem.eql(u8, "\nI'm a multiline string. \\You \nCan't see\n\nme", parsed));
}
test "Normal Quoted String" {
    var tf =
        \\"I'm a multiline \nstring.\U0010FFFF \\You "
    ;
    var parsed = try toString(test_allocator, tf);
    defer test_allocator.free(parsed);
    //std.log.warn("\nGOT: \n{s}\n", .{parsed});
    try testing.expect(std.mem.eql(u8, "I'm a multiline \nstring.\u{10FFFF} \\You ", parsed));
}
