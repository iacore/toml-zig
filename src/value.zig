const std = @import("std");
const test_allocator = std.testing.allocator;
const testing = std.testing;
const assert = std.debug.assert;
const Allocator = std.mem.Allocator;
const String = @import("Value/string.zig");
const Boolean = @import("Value/boolean.zig");
const DateTime = @import("Value/datetime.zig");
const Float = @import("Value/float.zig");
const Integer = @import("Value/integer.zig");
const ArrayList = std.ArrayList;
const StringArrayHashMap = std.StringArrayHashMap;
const Array = ArrayList(Value);
const ObjectMap = StringArrayHashMap(Value);

/// All tokens for the lexer. This union may include tokens that are not
/// processed during the
/// lexing process but during the parse process.
const Token = union(enum) {
    Comment,
    EOF,
    FeedStart,
    // =
    ObjectSeparator,
    //[
    // Fully evaluated in the second pass
    TableBegin,
    //]
    TableEnd,
    ArrayTableBegin,
    ArrayTableEnd,
    // [
    // Fully evaluated in the second pass
    ArrayBegin,
    // Array Comma
    Comma,
    Dot,
    // ]
    ArrayEnd,
    // {
    // Fully evaluated in the second pass
    InlineTableBegin,
    // }
    InlineTableEnd,
    // " or ' also """ or '''
    // Fully evaluated in the second pass
    String: struct {
        // count the ticks
        count: isize = 1,
        literal: bool = false,
    },
    // Full evaluated in the second pass
    Number,
    // f or t after = or ,
    // This will be evaluated in the second pass
    Bool,
    //DateTime: DateTime.DateTime,
    NewLine,
};
///Collection of errors for everything that's in this file. This include,
///lexing and parsing.
const ParserError = error{
    TwoEquals,
    BadFormat,
    BadCommaPlacement,
    TrailingCommaAfterLastKeyVal,
    NewLineInsideInlineTable,
    NewLineInsideTable,
    NewLineAfterEqual,
    // Currently left for the second pass to check if we have a key with new line
    NewLineBeforeEqual,
    UnseparatedEntries,
    UnexpectedCharacter,
    SpaceInKey,
    NotFound,
    TrailingDot,
    EmptyName,
    KeyWithNoValue,
    DuplicateKey,
    DuplicateTable,
    BadDotPlacement,
    TypeMismatch,
    UnexpectedCurlyBracket,
    ArrayLengthMismatch,
    UnexpectedBracket,
};

/// Iterator for the tokens.
pub const TokenStream = struct {
    // current index
    i: usize,
    // Slice gotten from @This.init()
    slice: []const u8,
    token: Token,
    // Counter for nested brackets
    nestedB: usize = 0,
    // Counter for nested squiglies
    nestedS: usize = 0,
    prevToken: ?Token = null,

    pub fn init(slice: []const u8) TokenStream {
        return TokenStream{
            .i = 0,
            .slice = slice,
            .token = .FeedStart,
        };
    }

    pub fn next(self: *TokenStream) !Token {
        defer self.i += 1;
        while (self.i < self.slice.len) : (self.i += 1) {
            switch (self.slice[self.i]) {
                '#' => {
                    if (self.token == .String) {
                        if (self.token.String.count != 2 and self.token.String.count < 6) {
                            continue;
                        }
                    }
                    self.prevToken = self.token;
                    self.token = .Comment;
                    return self.token;
                },
                '[' => {
                    if (self.token != .Comment) {
                        if (self.token == .String) {
                            if (self.token.String.count != 2 and self.token.String.count < 6) {
                                continue;
                            }
                        }
                        if (self.token == .TableBegin) {
                            self.token = .ArrayTableBegin;
                            return .ArrayTableBegin;
                        }
                        if (self.token == .ArrayTableBegin) {
                            return ParserError.BadFormat;
                        }
                        if (self.token == Token.ObjectSeparator or self.token == .ArrayBegin or self.nestedB > 0) {
                            self.token = .ArrayBegin;
                            self.nestedB += 1;
                            return Token.ArrayBegin;
                        }
                        if (self.token == .NewLine or self.token == .FeedStart) {
                            self.token = .TableBegin;
                            return Token.TableBegin;
                        }
                        return ParserError.UnexpectedBracket;
                    }
                },
                ']' => {
                    if (self.token != .Comment) {
                        if (self.token == .String) {
                            if (self.token.String.count != 2 and self.token.String.count < 6) {
                                continue;
                            }
                        }
                        if (self.token == .ArrayTableBegin) {
                            self.token = .ArrayTableEnd;
                            return .ArrayTableEnd;
                        }
                        if (self.nestedB > 0) {
                            self.nestedB -= 1;
                            self.token = .ArrayEnd;
                            return Token.ArrayEnd;
                        }
                        if (self.token == .ArrayTableEnd or self.token == .TableBegin or self.token == .String or self.token == .Dot) {
                            self.token = .TableEnd;
                            return Token.TableEnd;
                        }
                        if (self.nestedB == 0) {
                            return ParserError.UnexpectedBracket;
                        }
                        return ParserError.BadFormat;
                    }
                },
                '{' => {
                    if (self.token != .Comment) {
                        if (self.token == .String) {
                            if (self.token.String.count != 2 and self.token.String.count < 6) {
                                continue;
                            }
                        }
                        if (self.token == .ObjectSeparator or self.token == .ArrayBegin or self.token == .Comma) {
                            self.nestedS += 1;
                            self.token = .InlineTableBegin;
                            return Token.InlineTableBegin;
                        }
                        return ParserError.BadFormat;
                    }
                },
                '}' => {
                    if (self.token == .NewLine) {
                        return ParserError.NewLineInsideInlineTable;
                    }
                    if (self.token != .Comment) {
                        if (self.token == .String) {
                            if (self.token.String.count != 2 and self.token.String.count < 6) {
                                continue;
                            }
                        }
                        if (self.nestedS > 0) {
                            if (self.token == .Comma) {
                                return ParserError.TrailingCommaAfterLastKeyVal;
                            }
                            self.nestedS -= 1;
                            self.token = .InlineTableEnd;
                            return Token.InlineTableEnd;
                        }
                        return ParserError.UnexpectedCurlyBracket;
                    }
                },
                '=' => {
                    if (self.token == .ObjectSeparator) {
                        return ParserError.TwoEquals;
                    }
                    if (self.token != .Comment) {
                        //    if (self.token == .String) {
                        //        if (self.token.String.count != 2 and self.token.String.count < 6) {
                        //            self.token = Token.ObjectSeparator;
                        //            return Token.ObjectSeparator;
                        //        }
                        //    }
                        if (self.token != .Dot and self.token != .Comma and
                            self.token != .NewLine and self.token != .FeedStart and self.token != .InlineTableBegin and self.token != .Bool)
                        {
                            std.debug.print("Error: {}, in pos: {}\n", .{ self.token, self.i + 1 });
                            return ParserError.UnseparatedEntries;
                        }
                        self.token = Token.ObjectSeparator;
                        return Token.ObjectSeparator;
                    }
                },
                '"', '\'' => |c| {
                    if (self.token != .Comment) {
                        if (self.token == .ObjectSeparator or self.token == .Comma or self.token == .ArrayBegin) {
                            self.token = Token{ .String = .{ .literal = c == '\'' } };
                            return self.token;
                        }
                        if (self.token == Token.String) {
                            if ((self.token.String.literal and c == '\'') or (!self.token.String.literal and c == '"')) {
                                self.token.String.count += 1;
                                return self.token;
                            }
                        }
                    }
                },
                // 0x Counts here
                '0'...'9' => {
                    if (self.token == .TableBegin) {
                        continue;
                    }
                    if (self.token != .Comment) {
                        if (self.token == .String) {
                            if (self.token.String.count != 2 and self.token.String.count < 6) {
                                continue;
                            }
                        }
                        if (self.token == .TableEnd or self.token == .ArrayEnd or self.token == .ArrayTableEnd) {
                            std.debug.print("Error NUM: {}, in pos: {}\n", .{ self.token, self.i + 1 });
                            return ParserError.UnexpectedCharacter;
                        }
                        if (self.token == .ObjectSeparator) {
                            self.token = .Number;
                            return .Number;
                        }
                        var peeker = try self.peekNum();
                        if (self.token == .Comma or self.token == .ArrayBegin) {
                            if (peeker == .EOF) {
                                return ParserError.UnexpectedCharacter;
                            }
                            if (peeker != .ObjectSeparator) {
                                self.token = .Number;
                                return .Number;
                            }
                        }
                        if (self.token != .Number and peeker != .ObjectSeparator and self.token != .ArrayTableBegin and self.token != .Dot) {
                            return ParserError.UnexpectedCharacter;
                        }
                    }
                },
                '\n' => {
                    if (self.token == .Comment) {
                        self.token = self.prevToken.?;
                        if (self.token == .NewLine or self.token == .FeedStart) {
                            return self.token;
                        }
                    }
                    if (self.token == .String) {
                        // TODO: Perhaps a more correct way is to check for >= 6 and <= 8 and fail for anything else
                        if (self.token.String.count == 2 or self.token.String.count >= 6) {
                            self.token = .NewLine;
                            return .NewLine;
                        }
                        if (self.token.String.count > 3) {
                            self.token.String.count = 3;
                        }
                    }
                    if (self.token == .TableBegin) {
                        return ParserError.NewLineInsideTable;
                    }
                    if (self.token != .ArrayBegin and self.token != .String and
                        self.nestedS == 0 and self.nestedB == 0 and self.token != .NewLine and self.token != .ObjectSeparator)
                    {
                        self.token = .NewLine;
                        return .NewLine;
                    }
                    if (self.token == .InlineTableBegin or
                        (self.nestedS > 0 and self.token == .Comma))
                    {
                        return ParserError.NewLineInsideInlineTable;
                    }
                    if (self.token == .ObjectSeparator) {
                        return ParserError.NewLineAfterEqual;
                    }
                },
                ',' => {
                    if (self.token != .Comment) {
                        // Empty Token
                        if (self.token == .InlineTableBegin or self.token == .ArrayBegin or self.token == .Comma) {
                            return ParserError.BadCommaPlacement;
                        }
                        if (self.nestedB > 0 or self.nestedS > 0) {
                            if (self.token == .String) {
                                if (self.token.String.count == 2 or self.token.String.count >= 6) {
                                    self.token = .Comma;
                                    return .Comma;
                                } else {
                                    continue;
                                }
                            }
                            self.token = .Comma;
                            return .Comma;
                        }
                        if (self.token == .String) {
                            if (self.token.String.count != 2 and self.token.String.count < 6) {
                                continue;
                            }
                        }

                        return ParserError.BadCommaPlacement;
                    }
                },
                '.' => {
                    if (self.token != .Comment) {
                        if (self.token == .Dot or self.token == .FeedStart or self.token == .Comma or self.token == .NewLine) {
                            self.token = .Dot;
                            return .Dot;
                        }
                    }
                },
                't', 'f' => {
                    if (self.token == .TableBegin) {
                        continue;
                    }
                    if (self.token != .Comment) {
                        if (self.token == .String) {
                            if (self.token.String.count != 2 and self.token.String.count < 6) {
                                continue;
                            }
                        }
                        if (self.token == .TableEnd or self.token == .ArrayEnd or self.token == .ArrayTableEnd) {
                            std.debug.print("Error NUM: {}, in pos: {}\n", .{ self.token, self.i + 1 });
                            return ParserError.UnexpectedCharacter;
                        }
                        if (self.token == .ObjectSeparator) {
                            self.token = .Bool;
                            return .Bool;
                        }
                        var peeker = try self.peekNum();
                        if (self.token == .Comma or self.token == .ArrayBegin) {
                            if (peeker == .EOF) {
                                return ParserError.UnexpectedCharacter;
                            }
                            if (peeker != .ObjectSeparator) {
                                self.token = .Bool;
                                return .Bool;
                            }
                        }
                        if (self.token != .Number and peeker != .ObjectSeparator and self.token != .ArrayTableBegin and self.token != .Dot) {
                            return ParserError.UnexpectedCharacter;
                        }
                    }
                },
                ' ' => continue,
                else => {
                    if (self.token == .TableBegin or self.token == .Bool or self.token == .Number) {
                        continue;
                    }
                    if (self.token == .Comma) {
                        var peeker = try self.peekNum();
                        if (peeker != .ObjectSeparator) {
                            std.debug.print("Error: {}, in pos: {}\n", .{ self.token, self.i + 1 });
                            return ParserError.UnexpectedCharacter;
                        }
                        continue;
                    }
                    if (self.token == .NewLine) {
                        var peeker = try self.peekNum();
                        if (peeker != .ObjectSeparator or peeker == .NewLine) {
                            std.debug.print("Error: {}, in pos: {}\n", .{ self.token, self.i + 1 });
                            return ParserError.KeyWithNoValue;
                        }
                        continue;
                    }
                    if (self.token != .Comment) {
                        if (self.token == .String) {
                            if (self.token.String.count > 3) {
                                self.token.String.count = 3;
                            }
                            continue;
                        }
                        if (self.token == .ArrayEnd or self.token == .ArrayTableEnd or self.token == .TableEnd) {
                            std.debug.print("Error: {}, in pos: {}\n", .{ self.token, self.i + 1 });
                            return ParserError.UnexpectedCharacter;
                        }
                    }
                },
            }
        }
        self.token = .EOF;
        return .EOF;
    }
    pub fn rest(self: *TokenStream) []const u8 {
        return self.slice[self.i..];
    }
    ///  This function is designed for peeking thorugh the current location to check for uncertain
    ///  tokens such as Bools and Numbers when being in the key side.
    pub fn peekNum(self: *TokenStream) !Token {
        var idx = self.i;
        while (self.slice[idx] != '\n' and self.slice[idx] != '[' and
            self.slice[idx] != ',' and self.slice[idx] != '=' and
            self.slice[idx] != ']' and idx < self.slice.len - 1) : (idx += 1)
        {}
        //std.debug.print("###{c}####\n",.{self.slice[idx]});
        if (self.slice[idx] == '=') {
            return .ObjectSeparator;
        }
        if (self.slice[idx] == ',') {
            return .Comma;
        }
        if (self.slice[idx] == ']') {
            return .ArrayEnd;
        }
        if (self.slice[idx] == '\n') {
            return .NewLine;
        }

        return .EOF;
    }

    // Get the value of an object
    // takes an allocator for String, Array, Table, InlineTables.
    pub fn get(self: *TokenStream, alloc: Allocator, key: []const u8) !Value {
        // reset
        self.i = 0;
        self.nestedB = 0;
        self.nestedS = 0;
        self.prevToken = null;
        self.token = .FeedStart;
        var start = self.i;
        _ = try self.next();
        var candy: []const u8 = undefined;
        var candytype: Token = undefined;
        var stopNonTableCheck = false;
        while (!std.mem.eql(u8, key, candy)) : (_ = try self.next()) {
            // Check for trailing key
            if (self.token == .EOF) {
                if (self.i - start - 1 != 0) {
                    candy = std.mem.trim(u8, self.slice[start .. self.i - 1], " ");
                    candy = std.mem.trim(u8, candy, "\n");
                    candy = std.mem.trim(u8, candy, "\t");
                } else {
                    break;
                }
            }
            if (self.token == .ObjectSeparator and !stopNonTableCheck) {
                candy = std.mem.trim(u8, self.slice[start .. self.i - 1], " ");
                candy = std.mem.trim(u8, candy, "\t");
                // Enhancement: Create a function to check the key.
                // Especially after implementing the dot.
                if (std.mem.containsAtLeast(u8, candy, 1, "\n")) {
                    return ParserError.NewLineBeforeEqual;
                }
                if (std.mem.containsAtLeast(u8, candy, 1, " ") and candy[0] != '"') {
                    return ParserError.SpaceInKey;
                }
                if (candy[0] == '"' and candy[candy.len - 1] == '"') {
                    candy = std.mem.trim(u8, candy, "\"");
                }
            }
            if (self.token == .TableBegin) {
                stopNonTableCheck = true;
            }
            if (self.token == .Dot) {
                candy = std.mem.trim(u8, self.slice[start .. self.i - 1], " ");
                candy = std.mem.trim(u8, candy, "\t");
                if (std.mem.containsAtLeast(u8, candy, 1, " ") and candy[0] != '"') {
                    return ParserError.SpaceInKey;
                }
                if (std.mem.eql(u8, key, candy)) {
                    break;
                }
            }
            if (self.token == .TableEnd or self.token == .ArrayTableEnd) {
                candy = std.mem.trim(u8, self.slice[start .. self.i - 1], " ");
                candy = std.mem.trim(u8, candy, "\t");
                candy = std.mem.trim(u8, candy, "[");
                candy = std.mem.trim(u8, candy, "]");
                if (candy.len == 0) {
                    return ParserError.EmptyName;
                }
                if (candy[0] == '.' or candy[candy.len - 1] == '.')
                    return ParserError.TrailingDot;
                var splitshit = split(u8, candy);
                candy = splitshit.first();
                candy = std.mem.trim(u8, candy, " ");
                if (std.mem.containsAtLeast(u8, candy, 1, " ") and (candy[0] != '"' or candy[0] != '\'')) {
                    return ParserError.SpaceInKey;
                }
                if (candy.len == 1 and (candy[0] == '\'' or candy[0] == '\"')) {
                    return ParserError.BadFormat;
                }
                if (candy[0] == '\"' and candy[candy.len - 1] == '\"') {
                    candy = std.mem.trim(u8, candy, "\"");
                } else if (candy[0] == '\'' and candy[candy.len - 1] == '\'') {
                    candy = std.mem.trim(u8, candy, "\'");
                } else if ((candy[0] == '\"' and candy[candy.len - 1] == '\'') or (candy[0] == '\'' and candy[candy.len - 1] == '\"')) {
                    return ParserError.BadFormat;
                }
                if (std.mem.eql(u8, key, candy)) {
                    break;
                }
            }
            start = self.i;
        }
        if (self.token == .EOF) {
            return ParserError.NotFound;
        }
        candytype = self.token;
        // TODO: If contains valid dots, we should create an ObjectMap with everything after the first dot as a key and then ObjectMap.put(getHelper());
        return try self.getHelper(alloc, candytype, start);
    }
    // Main helper for get, this takes off after we find any match and will do recursion to find
    //the rest of the values. For example, an array will call a recursion for every value inside it.
    fn getHelper(self: *TokenStream, alloc: Allocator, candytype: Token, st: usize) !Value {
        var start = st;
        switch (candytype) {
            .String => {
                while (self.token != .EOF and self.token != .NewLine and
                    self.token != .Comma and self.token != .Comment and
                    self.token != .ArrayEnd and self.token != .InlineTableEnd)
                {
                    _ = try self.next();
                }
                var end = self.i;
                var obj = std.mem.trim(u8, self.slice[start .. end - 1], " ");
                obj = std.mem.trim(u8, obj, "\t");
                if (try String.isString(obj)) {
                    var ret = Value{ .String = try String.toString(alloc, obj) };
                    errdefer alloc.free(ret);
                    return ret;
                }
            },
            .Number => {
                while (self.token != .EOF and self.token != .NewLine and
                    self.token != .Comma and self.token != .ArrayEnd and
                    self.token != .InlineTableEnd)
                {
                    _ = try self.next();
                }
                if (self.token == .NewLine and self.nestedB > 0) {
                    start = self.i;
                }
                var end = self.i;
                var obj = std.mem.trim(u8, self.slice[start .. end - 1], " ");
                obj = std.mem.trim(u8, obj, "\n");
                obj = std.mem.trim(u8, obj, "\t");

                if (try DateTime.isDateOrTime(obj)) {
                    return Value{ .LocalDateTime = try DateTime.DateTime.setString(obj) };
                }
                if (Float.isFloat(obj)) {
                    return Value{ .Float = try Float.toFloat(f64, obj) };
                }
                if (try Integer.isInteger(obj)) {
                    return Value{ .Integer = try Integer.parseSigned(i64, obj) };
                }
            },
            .ArrayBegin => {
                // This is to keep reference of where we should end up at for nested arrays
                // so we stop after we are done with the inner array and prevent adding the next non nested entry to the nested array.
                // we already know the nestedB is at least 1
                var nestedBGoal = self.nestedB - 1;
                var ret = Value{ .Array = Array.init(alloc) };
                errdefer ret.deinit(alloc);
                while (self.token != .EOF and self.nestedB != nestedBGoal) {
                    _ = try self.next();
                    var start_pos = self.i - 1;
                    if (self.token == .Comment) {
                        continue;
                    }
                    if ((self.token != .ArrayEnd and self.nestedB > 0) and
                        self.token != .InlineTableEnd and self.token != .EOF and self.token != .Comma and self.token != .Comment)
                    {
                        try ret.Array.append(try self.getHelper(alloc, self.token, start_pos));
                    }
                }
                return ret;
            },
            .Bool => {
                while (self.token != .EOF and self.token != .NewLine and self.token != .Comma and
                    self.token != .ArrayEnd and self.token != .InlineTableEnd)
                {
                    _ = try self.next();
                }
                var end = self.i;
                var obj = std.mem.trim(u8, self.slice[start .. end - 1], " ");
                obj = std.mem.trim(u8, obj, "\n");
                obj = std.mem.trim(u8, obj, "\t");
                if (Boolean.isBoolean(obj)) {
                    return Value{ .Boolean = Boolean.toBool(obj) };
                } else {
                    std.debug.print("XXCZ{s}XZX\n", .{obj});
                    return ParserError.BadFormat;
                }
            },
            .InlineTableBegin => {
                var nestedSGoal = self.nestedS - 1;
                var ret = Value{ .Table = ObjectMap.init(alloc) };
                errdefer ret.deinit(alloc);
                // after {
                var start_pos = self.i;
                while (self.token != .EOF and self.token != .NewLine and self.nestedS != nestedSGoal) {
                    if (self.token == .Bool) {
                        self.i = start_pos;
                        _ = try self.next();
                        continue;
                    }
                    if (self.token == .ObjectSeparator) {
                        var key = std.mem.trim(u8, self.slice[start_pos .. self.i - 1], " ");
                        key = std.mem.trim(u8, key, "\t");
                        if (std.mem.containsAtLeast(u8, key, 1, "\n")) {
                            return ParserError.NewLineBeforeEqual;
                        }

                        if (key.len == 0) {
                            return ParserError.EmptyName;
                        }
                        if (key[0] == '.' or key[key.len - 1] == '.')
                            return ParserError.TrailingDot;
                        var splittedDots = split(u8, key);
                        var parent = splittedDots.first();
                        var isDotted = false;
                        var value_ptr: *Value = undefined;
                        if (!std.mem.eql(u8, key, parent)) {
                            var first = try ret.Table.getOrPut(parent);
                            first.value_ptr.* = Value{ .Table = ObjectMap.init(alloc) };
                            value_ptr = first.value_ptr;
                            isDotted = true;
                            while (splittedDots.next()) |part| {
                                var first_result = try value_ptr.Table.getOrPut(part);
                                first_result.value_ptr.* = Value{ .Table = ObjectMap.init(alloc) };
                                value_ptr = first_result.value_ptr;
                            }
                        }
                        if (std.mem.containsAtLeast(u8, key, 1, " ")) {
                            return ParserError.SpaceInKey;
                        }
                        start_pos = self.i;
                        _ = try self.next();
                        if (self.token != .InlineTableEnd and self.token != .NewLine and self.token != .EOF) {
                            if (isDotted) {
                                value_ptr.* = try self.getHelper(alloc, self.token, start_pos);
                            } else {
                                try ret.Table.put(key, try self.getHelper(alloc, self.token, start_pos));
                            }
                        }
                    } else {
                        start_pos = self.i;
                        _ = try self.next();
                    }
                }
                return ret;
            },
            .TableEnd, .ArrayTableEnd => {
                var array = false;
                var ret: Value = undefined;
                if (self.token == .ArrayTableEnd) {
                    ret = Value{ .Array = ArrayList(Value).init(alloc) };
                    array = true;
                } else {
                    ret = Value{ .Table = ObjectMap.init(alloc) };
                }
                errdefer ret.deinit(alloc);
                // process dots

                if (self.i - start - 1 == 0) {
                    return ParserError.EmptyName;
                }
                if (self.slice[start] == '.' or self.slice[self.i - 2] == '.')
                    return ParserError.TrailingDot;
                var splittedDots = split(u8, self.slice[start .. self.i - 1]);
                var parent = splittedDots.first();
                parent = std.mem.trim(u8, parent, " ");
                // Do first next
                var value_ptr: *Value = undefined;
                if (self.token == .ArrayTableEnd) {
                    try ret.Array.append(Value{ .Table = ObjectMap.init(alloc) });
                    value_ptr = &ret.Array.items[0];
                } else {
                    value_ptr = &ret;
                }
                while (splittedDots.next()) |prt| {
                    var part = std.mem.trim(u8, prt, " ");
                    if (part.len == 1 and (part[0] == '\'' or part[0] == '\"')) {
                        return ParserError.BadFormat;
                    }
                    if (part[0] == '\"' and part[part.len - 1] == '\"') {
                        part = std.mem.trim(u8, part, "\"");
                    } else if (part[0] == '\'' and part[part.len - 1] == '\'') {
                        part = std.mem.trim(u8, part, "\'");
                    } else if ((part[0] == '\"' and part[part.len - 1] == '\'') or (part[0] == '\'' and part[part.len - 1] == '\"')) {
                        return ParserError.BadFormat;
                    }
                    var first_result = try value_ptr.Table.getOrPut(part);
                    first_result.value_ptr.* = Value{ .Table = ObjectMap.init(alloc) };
                    value_ptr = first_result.value_ptr;
                }
                var start_pos = self.i;
                if (array) {
                    _ = try self.next();
                    start_pos = self.i;
                }
                var found_obj: []const u8 = parent;
                _ = try self.next();
                while (self.token != .EOF) {
                    if (self.token == .TableEnd or self.token == .ArrayTableEnd) {
                        var is_arr_table = self.token == .ArrayTableEnd;
                        var temp_obj = std.mem.trim(u8, self.slice[start_pos .. self.i - 1], " ");
                        var exact = std.mem.eql(u8, temp_obj, parent);

                        if (temp_obj.len == 0) {
                            return ParserError.EmptyName;
                        }
                        if (temp_obj[0] == '.' or temp_obj[temp_obj.len - 1] == '.')
                            return ParserError.TrailingDot;
                        var temp_split = split(u8, temp_obj);
                        found_obj = temp_split.first();
                        if (std.mem.eql(u8, found_obj, parent)) {
                            if (array and exact) {
                                try ret.Array.append(Value{ .Table = ObjectMap.init(alloc) });
                                var len = ret.Array.items.len;
                                value_ptr = &ret.Array.items[len - 1];
                            } else if (array and !exact) {
                                var len = ret.Array.items.len;
                                value_ptr = &ret.Array.items[len - 1];
                            } else if (!array) {
                                value_ptr = &ret;
                            }
                            var duplicate = false;
                            while (temp_split.next()) |prt| {
                                var part = prt;
                                if (part.len == 1 and (part[0] == '\'' or part[0] == '\"')) {
                                    return ParserError.BadFormat;
                                }
                                if (part[0] == '\"' and part[part.len - 1] == '\"') {
                                    part = std.mem.trim(u8, part, "\"");
                                } else if (part[0] == '\'' and part[part.len - 1] == '\'') {
                                    part = std.mem.trim(u8, part, "\'");
                                } else if ((part[0] == '\"' or part[part.len - 1] == '\"') or
                                    (part[0] == '\'' or part[part.len - 1] == '\''))
                                {
                                    return ParserError.BadFormat;
                                }
                                var dresult = try value_ptr.Table.getOrPut(part);
                                if (part.len == 0) {
                                    return ParserError.BadDotPlacement;
                                }
                                if (dresult.found_existing) {
                                    duplicate = true;
                                    value_ptr = dresult.value_ptr;
                                    continue;
                                }
                                if (is_arr_table) {
                                    dresult.value_ptr.* = Value{ .Array = ArrayList(Value).init(alloc) };
                                    try dresult.value_ptr.Array.append(Value{ .Table = ObjectMap.init(alloc) });
                                    value_ptr = &dresult.value_ptr.Array.items[0];
                                } else {
                                    dresult.value_ptr.* = Value{ .Table = ObjectMap.init(alloc) };
                                    value_ptr = dresult.value_ptr;
                                }
                                duplicate = false;
                            }
                            if (duplicate and is_arr_table) {
                                try value_ptr.Array.append(Value{ .Table = ObjectMap.init(alloc) });
                                value_ptr = &value_ptr.Array.items[value_ptr.Array.items.len - 1];
                            }
                            if (duplicate and !is_arr_table) {
                                return ParserError.DuplicateTable;
                            }
                        }
                        start_pos = self.i;
                        _ = try self.next();
                    }
                    if (self.token == .Comment) {
                        while (self.token != .NewLine) {
                            start_pos = self.i;
                            _ = try self.next();
                        }
                    }
                    var top_ptr = value_ptr;
                    if (self.token == .Bool) {
                        self.i = start_pos;
                        _ = try self.next();
                        continue;
                    }
                    if (self.token == .ObjectSeparator) {
                        // do dot split again
                        var key = std.mem.trim(u8, self.slice[start_pos .. self.i - 1], " ");
                        key = std.mem.trim(u8, key, "\t");
                        if (std.mem.containsAtLeast(u8, key, 1, "\n")) {
                            return ParserError.NewLineBeforeEqual;
                        }
                        if (std.mem.containsAtLeast(u8, key, 1, " ")) {
                            return ParserError.SpaceInKey;
                        }
                        //TODO: Switch to manual split to prevent splitting from inside quotes

                        if (key.len == 0) {
                            return ParserError.EmptyName;
                        }
                        if (key[0] == '.' or key[key.len - 1] == '.')
                            return ParserError.TrailingDot;
                        var dottedKeySplit = split(u8, key);
                        var first_part = dottedKeySplit.first();
                        var last_part: []const u8 = key;
                        var inDot = false;
                        if (first_part.len == key.len) {
                            if (key[0] == '\"' and key[key.len - 1] == '\"') {
                                last_part = std.mem.trim(u8, last_part, "\"");
                            } else if (key[0] == '\'' and key[key.len - 1] == '\'') {
                                last_part = std.mem.trim(u8, last_part, "\'");
                            } else if ((key[0] == '\"' or key[key.len - 1] == '\"') or (key[0] == '\'' or key[key.len - 1] == '\'')) {
                                return ParserError.BadFormat;
                            }
                        } else {
                            inDot = true;
                            dottedKeySplit.reset();
                            while (dottedKeySplit.next()) |nextT| {
                                var next_table = nextT;
                                if (next_table.len == 0) {
                                    return ParserError.BadDotPlacement;
                                }

                                if (next_table[0] == '\"' and next_table[next_table.len - 1] == '\"') {
                                    next_table = std.mem.trim(u8, next_table, "\"");
                                } else if (next_table[0] == '\'' and next_table[next_table.len - 1] == '\'') {
                                    next_table = std.mem.trim(u8, next_table, "\'");
                                } else if ((next_table[0] == '\"' or next_table[next_table.len - 1] == '\"') or (next_table[0] == '\'' or next_table[next_table.len - 1] == '\'')) {
                                    return ParserError.BadFormat;
                                }

                                var kresult = try value_ptr.Table.getOrPut(next_table);
                                kresult.value_ptr.* = Value{ .Table = ObjectMap.init(alloc) };
                                value_ptr = kresult.value_ptr;
                                last_part = next_table;
                            }
                        }
                        start_pos = self.i;
                        _ = try self.next();

                        if (self.token != .EOF and self.token != .TableBegin) {
                            if (std.mem.eql(u8, found_obj, parent)) {
                                if (!inDot) {
                                    const result = try value_ptr.Table.getOrPut(last_part);
                                    if (result.found_existing) {
                                        return ParserError.DuplicateKey;
                                    }
                                    result.value_ptr.* = try self.getHelper(alloc, self.token, start_pos);
                                } else {
                                    value_ptr.* = try self.getHelper(alloc, self.token, start_pos);
                                }
                            }
                        }

                        value_ptr = top_ptr;
                    } else {
                        if (self.token != .Dot) {
                            start_pos = self.i;
                        }
                        _ = try self.next();
                        if (self.token == .Comment) {
                            start_pos = self.i;
                        }
                    }
                }
                return ret;
            },
            .Dot => {
                var ret = Value{ .Table = ObjectMap.init(alloc) };
                errdefer ret.deinit(alloc);
                var start_pos = start;
                var value_ptr: *Value = undefined;
                var key = std.mem.trim(u8, self.slice[start_pos .. self.i - 1], " ");
                key = std.mem.trim(u8, key, "\t");
                if (std.mem.containsAtLeast(u8, key, 1, "\n")) {
                    return ParserError.NewLineBeforeEqual;
                }
                if (std.mem.containsAtLeast(u8, key, 1, " ")) {
                    return ParserError.SpaceInKey;
                }
                var result = try ret.Table.getOrPut(key);
                result.value_ptr.* = Value{ .Table = ObjectMap.init(alloc) };
                value_ptr = result.value_ptr;
                start_pos = self.i;
                _ = try self.next();
                while (self.token != .ObjectSeparator) : (_ = try self.next()) {
                    var key2 = std.mem.trim(u8, self.slice[start_pos .. self.i - 1], " ");
                    key2 = std.mem.trim(u8, key2, "\t");
                    if (key2.len == 0) {
                        continue;
                    }
                    if (std.mem.containsAtLeast(u8, key2, 1, "\n")) {
                        return ParserError.NewLineBeforeEqual;
                    }
                    if (std.mem.containsAtLeast(u8, key2, 1, " ")) {
                        return ParserError.SpaceInKey;
                    }
                    var dresult = try value_ptr.Table.getOrPut(key2);
                    dresult.value_ptr.* = Value{ .Table = ObjectMap.init(alloc) };
                    value_ptr = dresult.value_ptr;
                    start_pos = self.i;
                }
                // We reached =
                var key3 = std.mem.trim(u8, self.slice[start_pos .. self.i - 1], " ");
                key3 = std.mem.trim(u8, key3, "\t");
                if (std.mem.containsAtLeast(u8, key3, 1, "\n")) {
                    return ParserError.NewLineBeforeEqual;
                }
                if (key3.len > 0) {
                    var dresult = try value_ptr.Table.getOrPut(key3);
                    dresult.value_ptr.* = Value{ .Table = ObjectMap.init(alloc) };
                    value_ptr = dresult.value_ptr;
                    start_pos = self.i;
                    _ = try self.next();
                    while (self.token == .NewLine) : (_ = try self.next()) {
                        start_pos = self.i;
                    }
                    if (self.token == .ObjectSeparator) {
                        start_pos = self.i;
                        _ = try self.next();
                    }
                    value_ptr.* = try self.getHelper(alloc, self.token, start_pos);
                }
                return ret;
            },
            else => {
                std.debug.print("@@@{}@@@\n", .{candytype});
                unreachable;
            },
        }
        unreachable;
    }
};

const Value = union(enum) {
    String: []const u8,
    Integer: i64,
    Float: f64,
    Boolean: bool,
    LocalDateTime: DateTime.DateTime,
    Array: Array,
    // Table/Inline Table
    Table: ObjectMap,

    pub fn deinit(self: *Value, alloc: Allocator) void {
        switch (self.*) {
            .Array => |*arr| {
                for (arr.items) |*item| {
                    item.deinit(alloc);
                }
                arr.deinit();
            },
            .Table => |*table| {
                for (table.values()) |*val| {
                    val.deinit(alloc);
                }
                table.deinit();
            },
            .String => |s| {
                alloc.free(s);
            },
            else => return,
        }
    }
};

// Utilizes get() to get the HashMap of the desired struct.
// User will have to manually deinit any fields that are allocated.
// Allocated Items are: Strings, Arrays and perhaps HashMaps (Inline Tables)
fn parse(comptime T: type, allocator: Allocator, input: []const u8) !T {
    var tokenstream = TokenStream.init(input);
    var r: T = undefined;
    switch (@typeInfo(T)) {
        .Struct => |st| {
            inline for (st.fields) |field| {
                const ftype: type = field.type;
                var getReturn: Value = try tokenstream.get(allocator, field.name);
                defer getReturn.deinit(allocator);
                @field(r, field.name) = try parseHelper(ftype, allocator, getReturn);
            }
        },
        else => unreachable,
    }
    return r;
}

pub fn Parsed(comptime T: type) type {
    return struct {
        arena: std.heap.ArenaAllocator,
        value: T,

        pub fn deinit(this: @This()) void {
            this.arena.deinit();
        }
    };
}

pub fn parseAlloc(comptime T: type, allocator: Allocator, input: []const u8) !Parsed(T) {
    var arena = std.heap.ArenaAllocator.init(allocator);

    var tokenstream = TokenStream.init(input);
    var r: T = undefined;
    switch (@typeInfo(T)) {
        .Struct => |st| {
            inline for (st.fields) |field| {
                const ftype: type = field.type;
                var getReturn: Value = try tokenstream.get(arena.allocator(), field.name);
                defer getReturn.deinit(arena.allocator());
                @field(r, field.name) = try parseHelper(ftype, arena.allocator(), getReturn);
            }
        },
        else => unreachable,
    }
    return .{ .arena = arena, .value = r };
}

fn parseHelper(comptime T: type, allocator: Allocator, val: Value) !T {
    switch (val) {
        .Boolean => |ret| {
            if (T == bool) {
                return ret;
            } else {
                return ParserError.TypeMismatch;
            }
        },
        .String => |ret| {
            const info = @typeInfo(T);
            if (info == .Pointer and info.Pointer.size == .Slice and info.Pointer.child == u8) {
                const sentinel = if (info.Pointer.sentinel) |sentinel_ptr|
                    @as(*const info.Pointer.child, @ptrCast(sentinel_ptr)).*
                else
                    null;
                var yield = try allocator.allocWithOptions(u8, ret.len, null, sentinel);
                std.mem.copy(u8, yield, ret);
                return yield;
            } else {
                return ParserError.TypeMismatch;
            }
        },
        .Table => |ret| {
            const info = @typeInfo(T);
            if (info == .Struct) {
                var r: T = undefined;
                inline for (info.Struct.fields) |field| {
                    @field(r, field.name) = try parseHelper(field.type, allocator, ret.get(field.name).?);
                }
                return r;
            }
        },
        .Array => |arr| {
            const info = @typeInfo(T);
            if (info == .Pointer) {
                if (info.Pointer.size == .Slice) {
                    const ctype = info.Pointer.child;
                    var array = ArrayList(ctype).init(allocator);
                    for (arr.items) |item| {
                        var temp = try parseHelper(ctype, allocator, item);
                        try array.append(temp);
                    }
                    if (info.Pointer.sentinel) |sentinel_ptr| {
                        const sentinel = @as(*const info.Pointer.child, @ptrCast(sentinel_ptr)).*;
                        return try array.toOwnedSliceSentinel(sentinel);
                    } else return array.toOwnedSlice();
                }
            }
            if (info == .Array) {
                if (info.Array.len != arr.items.len) {
                    return ParserError.ArrayLengthMismatch;
                }
                const ctype = info.Array.child;
                var array: [info.Array.len]ctype = undefined;
                for (arr.items, 0..) |item, idx| {
                    var temp = try parseHelper(ctype, allocator, item);
                    array[idx] = temp;
                }
                return array;
            }
            return ParserError.TypeMismatch;
        },
        .Integer => |ret| {
            if (T == i64) {
                return ret;
            } else if (@typeInfo(T) == .Int) {
                return @as(T, @intCast(ret));
            }
        },
        .Float => |ret| {
            if (T == f64) {
                return ret;
            } else if (@typeInfo(T) == .Float) {
                return @as(T, @intCast(ret));
            }
        },
        else => unreachable,
    }
    return ParserError.TypeMismatch;
}
///Serialzes a given struct to a TOML string.
///For now, it's not possible to do inline tables or serialize HashMaps.
///for examples, check the test: basic serialization and nested struct serialization.
pub fn serialize(comptime T: type, input: T, allocator: Allocator, inner: bool) !ArrayList(u8) {
    var retVal = ArrayList(u8).init(allocator);
    const info = @typeInfo(T);
    switch (info) {
        .Struct => |st| {
            if (!inner) {
                try retVal.append('[');
                var stringified = std.mem.splitBackwards(u8, @typeName(T), ".");
                try retVal.appendSlice(stringified.first());
                try retVal.appendSlice("]\n");
            }
            inline for (st.fields) |field| {
                try retVal.appendSlice(field.name);
                try retVal.appendSlice(" = ");
                switch (@typeInfo(field.type)) {
                    .Int, .Float => {
                        var stringedVal = try std.fmt.allocPrint(allocator, "{}", .{@field(input, field.name)});
                        defer allocator.free(stringedVal);
                        try retVal.appendSlice(stringedVal);
                    },
                    .Pointer => |pt| {
                        if (pt.child == u8) {
                            var fv = @field(input, field.name);
                            if (std.mem.containsAtLeast(u8, fv, 1, "\n")) {
                                var stringedVal = try std.fmt.allocPrint(allocator, "\"\"\"{s}\"\"\"", .{fv});
                                defer allocator.free(stringedVal);
                                try retVal.appendSlice(stringedVal);
                            } else {
                                var stringedVal = try std.fmt.allocPrint(allocator, "\"{s}\"", .{@field(input, field.name)});
                                defer allocator.free(stringedVal);
                                try retVal.appendSlice(stringedVal);
                            }
                        } else if (@typeInfo(pt.child) == .Int) {
                            var al: field.field_type = @field(input, field.name);
                            try retVal.appendSlice("[");
                            if (al.len != 0) {
                                var first_val = try std.fmt.allocPrint(allocator, "{}", .{al[0]});
                                try retVal.appendSlice(first_val);
                                allocator.free(first_val);
                                for (al[1..]) |item| {
                                    var stringedVal = try std.fmt.allocPrint(allocator, ", {}", .{item});
                                    try retVal.appendSlice(stringedVal);
                                    defer allocator.free(stringedVal);
                                }
                            }
                            try retVal.appendSlice("]");
                        }
                    },
                    //                    ArrayList(u8), ArrayList(u16), ArrayList(u32), ArrayList(u64), ArrayList(u128), ArrayList(usize), ArrayList(i8), ArrayList(i16), ArrayList(i32), ArrayList(i64), ArrayList(i128), ArrayList(isize), ArrayList(f16), ArrayList(f32), ArrayList(f64), ArrayList(f128) => |typer| {
                    //                        var al: typer = @field(input, field.name);
                    //                        try retVal.appendSlice("[");
                    //                        if (al.items.len != 0) {
                    //                            var first_val = try std.fmt.allocPrint(allocator, "{}", .{al.items[0]});
                    //                            try retVal.appendSlice(first_val);
                    //                            allocator.free(first_val);
                    //                            for (al.items[1..]) |item| {
                    //                                var stringedVal = try std.fmt.allocPrint(allocator, ", {}", .{item});
                    //                                try retVal.appendSlice(stringedVal);
                    //                                defer allocator.free(stringedVal);
                    //                            }
                    //                        }
                    //                    },
                    .Array => {
                        var al: field.type = @field(input, field.name);
                        try retVal.appendSlice("[");
                        if (al.len != 0) {
                            var first_val = try std.fmt.allocPrint(allocator, "{}", .{al[0]});
                            try retVal.appendSlice(first_val);
                            allocator.free(first_val);
                            for (al[1..]) |item| {
                                var stringedVal = try std.fmt.allocPrint(allocator, ", {}", .{item});
                                try retVal.appendSlice(stringedVal);
                                defer allocator.free(stringedVal);
                            }
                        }
                        try retVal.appendSlice("]");
                    },
                    .Struct => {
                        var inner_struct = try serialize(field.type, @field(input, field.name), allocator, true);
                        try retVal.append('{');
                        try retVal.append(' ');
                        try retVal.appendSlice(inner_struct.items);
                        _ = retVal.pop();
                        _ = retVal.pop();
                        try retVal.appendSlice(" }");
                        try retVal.append('\n');
                        inner_struct.deinit();
                    },
                    else => |s| {
                        std.debug.print("@@{}@@\n", .{s});
                        unreachable;
                    },
                }
                if (!inner) {
                    try retVal.append('\n');
                } else {
                    try retVal.appendSlice(", ");
                }
            }
        },
        else => unreachable,
    }
    return retVal;
}

// Basic test for functionality, will no longer be needed once all functionalities are done
test "Basic Value" {
    var string_inner =
        \\ """HELLO""
    ;
    var string = Value{ .String = string_inner };
    _ = string;
}

pub fn SplitIterator(comptime T: type) type {
    return struct {
        buffer: []const T,
        index: usize,

        const Self = @This();
        fn reset(self: *Self) void {
            self.index = 0;
        }
        fn first(self: *Self) []const u8 {
            assert(self.index == 0);
            return self.next().?;
        }
        fn next(self: *Self) ?[]const u8 {
            if (self.index >= self.buffer.len) {
                return null;
            }
            defer self.index += 1;
            var start = self.index;
            var quote = false;
            while ((self.buffer[self.index] != '.' or quote == true)) : (self.index += 1) {
                if (self.index == self.buffer.len - 1) {
                    self.index += 1;
                    break;
                }
                if (self.buffer[self.index] == '\"') quote = !quote;
            }
            return self.buffer[start..self.index];
        }
    };
}
fn split(comptime T: type, input: []const T) SplitIterator(T) {
    return .{ .buffer = input, .index = 0 };
}

// Basic test for functionality, will no longer be needed once all functionalities are done
test "Basic Value - Array" {
    var t = Array.init(test_allocator);
    defer t.deinit();
    try t.append(.{ .String = "HELLO" });
    try t.append(.{ .Integer = 69 });
    var string: Value = .{ .Array = t };
    _ = string;
}

// Basic Parsing/Token Test
test "Basic Value - Parsing/Token" {
    var string_inner =
        \\[Hello]
        \\world = """HELLO
        \\"""
        \\test = 123
    ;
    var tokenstream = TokenStream.init(string_inner);
    try testing.expectEqual(Token.TableBegin, try tokenstream.next());
    try testing.expectEqual(Token.TableEnd, try tokenstream.next());
    try testing.expectEqual(Token.NewLine, try tokenstream.next());
    try testing.expectEqual(Token.ObjectSeparator, try tokenstream.next());
    try testing.expectEqual(Token{ .String = .{ .count = 1 } }, try tokenstream.next());
    var resteql =
        \\""HELLO
        \\"""
        \\test = 123
    ;
    try testing.expect(std.mem.eql(u8, resteql, tokenstream.rest()));
}

test "Actual TOML test" {
    const test_string =
        \\title = "TOML Example"
        \\[owner]
        \\name = "Tom Preston-Werner"
        \\dob = 1979-05-27T07:32:00-08:00
        \\
        \\[database]
        \\enabled = true
        \\ports = [ 8000, 8001, 8002 ]
        \\data = [ ["delta", "phi"], [3.14] ]
        \\temp_targets = { cpu = 79.5, case = 72.0 }
        \\
        \\[servers]
    ;

    var tokenstream = TokenStream.init(test_string);
    try testing.expectEqual(try tokenstream.next(), Token.ObjectSeparator);
    try testing.expectEqual(try tokenstream.next(), Token{ .String = .{ .count = 1 } });
    try testing.expectEqual(try tokenstream.next(), Token{ .String = .{ .count = 2 } });
    try testing.expectEqual(try tokenstream.next(), Token.NewLine);
    try testing.expectEqual(try tokenstream.next(), Token.TableBegin);
    try testing.expectEqual(try tokenstream.next(), Token.TableEnd);
    try testing.expectEqual(try tokenstream.next(), Token.NewLine);
    try testing.expectEqual(try tokenstream.next(), Token.ObjectSeparator);
    try testing.expectEqual(try tokenstream.next(), Token{ .String = .{ .count = 1 } });
    try testing.expectEqual(try tokenstream.next(), Token{ .String = .{ .count = 2 } });
    try testing.expectEqual(try tokenstream.next(), Token.NewLine);
    try testing.expectEqual(try tokenstream.next(), Token.ObjectSeparator);
    try testing.expectEqual(try tokenstream.next(), Token.Number);
    try testing.expectEqual(try tokenstream.next(), Token.NewLine);
    //[database]
    try testing.expectEqual(try tokenstream.next(), Token.TableBegin);
    try testing.expectEqual(try tokenstream.next(), Token.TableEnd);
    try testing.expectEqual(try tokenstream.next(), Token.NewLine);
    try testing.expectEqual(try tokenstream.next(), Token.ObjectSeparator);
    try testing.expectEqual(try tokenstream.next(), Token.Bool);
    try testing.expectEqual(try tokenstream.next(), Token.NewLine);
    try testing.expectEqual(try tokenstream.next(), Token.ObjectSeparator);
    try testing.expectEqual(try tokenstream.next(), Token.ArrayBegin);
    try testing.expectEqual(try tokenstream.next(), Token.Number);
    try testing.expectEqual(try tokenstream.next(), Token.Comma);
    try testing.expectEqual(try tokenstream.next(), Token.Number);
    try testing.expectEqual(try tokenstream.next(), Token.Comma);
    try testing.expectEqual(try tokenstream.next(), Token.Number);
    try testing.expectEqual(try tokenstream.next(), Token.ArrayEnd);
    try testing.expectEqual(try tokenstream.next(), Token.NewLine);
    try testing.expectEqual(try tokenstream.next(), Token.ObjectSeparator);
    try testing.expectEqual(try tokenstream.next(), Token.ArrayBegin);
    try testing.expectEqual(try tokenstream.next(), Token.ArrayBegin);
    try testing.expectEqual(tokenstream.nestedB, 2);
    try testing.expectEqual(try tokenstream.next(), Token{ .String = .{ .count = 1 } });
    try testing.expectEqual(try tokenstream.next(), Token{ .String = .{ .count = 2 } });
    try testing.expectEqual(try tokenstream.next(), Token.Comma);
    try testing.expectEqual(try tokenstream.next(), Token{ .String = .{ .count = 1 } });
    try testing.expectEqual(try tokenstream.next(), Token{ .String = .{ .count = 2 } });
    try testing.expectEqual(try tokenstream.next(), Token.ArrayEnd);
    try testing.expectEqual(try tokenstream.next(), Token.Comma);
    try testing.expectEqual(try tokenstream.next(), Token.ArrayBegin);
    try testing.expectEqual(try tokenstream.next(), Token.Number);
    try testing.expectEqual(try tokenstream.next(), Token.ArrayEnd);
    try testing.expectEqual(try tokenstream.next(), Token.ArrayEnd);
    try testing.expectEqual(try tokenstream.next(), Token.NewLine);
    try testing.expectEqual(try tokenstream.next(), Token.ObjectSeparator);
    try testing.expectEqual(try tokenstream.next(), Token.InlineTableBegin);
    try testing.expectEqual(try tokenstream.next(), Token.ObjectSeparator);
    try testing.expectEqual(try tokenstream.next(), Token.Number);
    try testing.expectEqual(try tokenstream.next(), Token.Comma);
    try testing.expectEqual(try tokenstream.next(), Token.ObjectSeparator);
    try testing.expectEqual(try tokenstream.next(), Token.Number);
    try testing.expectEqual(try tokenstream.next(), Token.InlineTableEnd);
    try testing.expectEqual(try tokenstream.next(), Token.NewLine);
    try testing.expectEqual(try tokenstream.next(), Token.TableBegin);
    try testing.expectEqual(try tokenstream.next(), Token.TableEnd);
    try testing.expectEqual(try tokenstream.next(), Token.EOF);
}

test "Not Error5" {
    const test_string =
        \\obj = """
        \\"
        \\HEL"LO"""""
        \\
    ;

    var tokenstream = TokenStream.init(test_string);
    try testing.expectEqual(try tokenstream.next(), Token.ObjectSeparator);
    try testing.expectEqual(try tokenstream.next(), Token{ .String = .{ .count = 1 } });
    try testing.expectEqual(try tokenstream.next(), Token{ .String = .{ .count = 2 } });
    try testing.expectEqual(try tokenstream.next(), Token{ .String = .{ .count = 3 } });
    try testing.expectEqual(try tokenstream.next(), Token{ .String = .{ .count = 4 } });
    try testing.expectEqual(try tokenstream.next(), Token{ .String = .{ .count = 4 } });
    try testing.expectEqual(try tokenstream.next(), Token{ .String = .{ .count = 4 } });
    try testing.expectEqual(try tokenstream.next(), Token{ .String = .{ .count = 5 } });
    try testing.expectEqual(try tokenstream.next(), Token{ .String = .{ .count = 6 } });
    try testing.expectEqual(try tokenstream.next(), Token{ .String = .{ .count = 7 } });
    try testing.expectEqual(try tokenstream.next(), Token{ .String = .{ .count = 8 } });
    try testing.expectEqual(try tokenstream.next(), Token.NewLine);
}
test "Not Error" {
    const test_string =
        \\obj = ""
        \\quot15 = '''Here are, fifteen quotation marks: """""""""""""""'''
        \\
    ;

    var tokenstream = TokenStream.init(test_string);
    try testing.expectEqual(Token.ObjectSeparator, try tokenstream.next());
    try testing.expectEqual(Token{ .String = .{ .count = 1 } }, try tokenstream.next());
    try testing.expectEqual(Token{ .String = .{ .count = 2 } }, try tokenstream.next());
    try testing.expectEqual(Token.NewLine, try tokenstream.next());
    try testing.expectEqual(Token.ObjectSeparator, try tokenstream.next());
    try testing.expectEqual(Token{ .String = .{ .count = 1, .literal = true } }, try tokenstream.next());
    try testing.expectEqual(Token{ .String = .{ .count = 2, .literal = true } }, try tokenstream.next());
    try testing.expectEqual(Token{ .String = .{ .count = 3, .literal = true } }, try tokenstream.next());
    try testing.expectEqual(Token{ .String = .{ .count = 4, .literal = true } }, try tokenstream.next());
    try testing.expectEqual(Token{ .String = .{ .count = 5, .literal = true } }, try tokenstream.next());
    try testing.expectEqual(Token{ .String = .{ .count = 6, .literal = true } }, try tokenstream.next());
    try testing.expectEqual(Token.NewLine, try tokenstream.next());
}
test "Not Error69" {
    const test_string =
        \\quot15 =  { hello = [1,2
        \\,3
        \\]  }
        \\
    ;

    var tokenstream = TokenStream.init(test_string);
    try testing.expectEqual(Token.ObjectSeparator, try tokenstream.next());
    try testing.expectEqual(Token.InlineTableBegin, try tokenstream.next());
    try testing.expectEqual(Token.ObjectSeparator, try tokenstream.next());
    try testing.expectEqual(Token.ArrayBegin, try tokenstream.next());
    try testing.expectEqual(Token.Number, try tokenstream.next());
    try testing.expectEqual(Token.Comma, try tokenstream.next());
    try testing.expectEqual(Token.Number, try tokenstream.next());
    try testing.expectEqual(Token.Comma, try tokenstream.next());
    try testing.expectEqual(Token.Number, try tokenstream.next());
    try testing.expectEqual(Token.ArrayEnd, try tokenstream.next());
    try testing.expectEqual(Token.InlineTableEnd, try tokenstream.next());
    try testing.expectEqual(Token.NewLine, try tokenstream.next());
}
test "Trailing Comma after InlineTable" {
    const test_string =
        \\quot15 =  { hello = """
        \\Hello, World
        \\""",}
        \\
    ;

    var tokenstream = TokenStream.init(test_string);
    try testing.expectEqual(Token.ObjectSeparator, try tokenstream.next());
    try testing.expectEqual(Token.InlineTableBegin, try tokenstream.next());
    try testing.expectEqual(Token.ObjectSeparator, try tokenstream.next());
    try testing.expectEqual(Token{ .String = .{ .count = 1 } }, try tokenstream.next());
    try testing.expectEqual(Token{ .String = .{ .count = 2 } }, try tokenstream.next());
    try testing.expectEqual(Token{ .String = .{ .count = 3 } }, try tokenstream.next());
    try testing.expectEqual(Token{ .String = .{ .count = 4 } }, try tokenstream.next());
    try testing.expectEqual(Token{ .String = .{ .count = 5 } }, try tokenstream.next());
    try testing.expectEqual(Token{ .String = .{ .count = 6 } }, try tokenstream.next());
    try testing.expectEqual(Token.Comma, try tokenstream.next());
    try testing.expectError(ParserError.TrailingCommaAfterLastKeyVal, tokenstream.next());
}

test "Testing InlineTable within array" {
    const test_string =
        \\quot15 = [123, {name = "Hello"}, ]
        \\
    ;

    var tokenstream = TokenStream.init(test_string);
    try testing.expectEqual(Token.ObjectSeparator, try tokenstream.next());
    try testing.expectEqual(Token.ArrayBegin, try tokenstream.next());
    try testing.expectEqual(Token.Number, try tokenstream.next());
    try testing.expectEqual(Token.Comma, try tokenstream.next());
    try testing.expectEqual(Token.InlineTableBegin, try tokenstream.next());
    try testing.expectEqual(Token.ObjectSeparator, try tokenstream.next());
    try testing.expectEqual(Token{ .String = .{ .count = 1 } }, try tokenstream.next());
    try testing.expectEqual(Token{ .String = .{ .count = 2 } }, try tokenstream.next());
    try testing.expectEqual(Token.InlineTableEnd, try tokenstream.next());
    try testing.expectEqual(Token.Comma, try tokenstream.next());
    try testing.expectEqual(Token.ArrayEnd, try tokenstream.next());
}
test "InlineTable With NewLine" {
    const test_string =
        \\quot15 = { hello = [1,2
        \\,3
        \\,] }
        \\
    ;

    var tokenstream = TokenStream.init(test_string);
    try testing.expectEqual(Token.ObjectSeparator, try tokenstream.next());
    try testing.expectEqual(Token.InlineTableBegin, try tokenstream.next());
    try testing.expectEqual(Token.ObjectSeparator, try tokenstream.next());
    try testing.expectEqual(Token.ArrayBegin, try tokenstream.next());
    try testing.expectEqual(Token.Number, try tokenstream.next());
    try testing.expectEqual(Token.Comma, try tokenstream.next());
    try testing.expectEqual(Token.Number, try tokenstream.next());
    try testing.expectEqual(Token.Comma, try tokenstream.next());
    try testing.expectEqual(Token.Number, try tokenstream.next());
    try testing.expectEqual(Token.Comma, try tokenstream.next());
    try testing.expectEqual(Token.ArrayEnd, try tokenstream.next());
    try testing.expectEqual(Token.InlineTableEnd, try tokenstream.next());
    try testing.expectEqual(Token.NewLine, try tokenstream.next());
}

test "Invalid" {
    const test_string =
        \\first = "Tom" last = "Preston-Werner"
    ;

    var tokenstream = TokenStream.init(test_string);
    try testing.expectEqual(Token.ObjectSeparator, try tokenstream.next());
    _ = try tokenstream.next();
    _ = try tokenstream.next();
    try testing.expectError(ParserError.UnseparatedEntries, tokenstream.next());
}

test "Get Object" {
    const test_string =
        \\hello = "123"
        \\world = 1234
        \\
    ;

    var tokenstream = TokenStream.init(test_string);
    var val = try tokenstream.get(test_allocator, "hello");
    defer val.deinit(test_allocator);
    try testing.expect(std.mem.eql(u8, val.String, "123"));
    var intTest = try tokenstream.get(test_allocator, "world");
    try testing.expectEqual(@as(i64, 1234), intTest.Integer);
}
test "Get Array of Booleans" {
    const test_string =
        \\hello = "123"
        \\world = [true, [false, true],]
        \\
    ;

    var tokenstream = TokenStream.init(test_string);
    var arr = try tokenstream.get(test_allocator, "world");
    defer arr.deinit(test_allocator);
    try testing.expectEqual(true, arr.Array.items[0].Boolean);
    try testing.expectEqual(false, arr.Array.items[1].Array.items[0].Boolean);
    try testing.expectEqual(true, arr.Array.items[1].Array.items[1].Boolean);
}
test "Get Array of Numbers" {
    const test_string =
        \\hello = "123"
        \\world = [123, [69, 420, {hello = 123 } ],123]
        \\
    ;

    var tokenstream = TokenStream.init(test_string);
    var arr = try tokenstream.get(test_allocator, "world");
    defer arr.deinit(test_allocator);
    try testing.expectEqual(@as(i64, 123), arr.Array.items[0].Integer);
    try testing.expectEqual(@as(i64, 123), arr.Array.items[2].Integer);
    try testing.expectEqual(@as(i64, 69), arr.Array.items[1].Array.items[0].Integer);
    try testing.expectEqual(@as(i64, 420), arr.Array.items[1].Array.items[1].Integer);
    try testing.expectEqual(@as(i64, 123), arr.Array.items[1].Array.items[2].Table.get("hello").?.Integer);
}

test "Get InlineTable of Numbers" {
    const test_string =
        \\hello = "123"
        \\world = { hello = 123, world = [123,], zawardo = 5}
        \\"HELL ad" = 144
        \\
    ;

    var tokenstream = TokenStream.init(test_string);
    var arr = try tokenstream.get(test_allocator, "world");
    //var dd = try tokenstream.get(test_allocator, "hello");
    //defer dd.deinit(test_allocator);
    defer arr.deinit(test_allocator);
    //try testing.expectEqual(arr.Table.get("hello").?.Integer, 123);
    try testing.expectEqual(@as(i64, 123), arr.Table.get("world").?.Array.items[0].Integer);
    try testing.expectEqual(@as(i64, 5), arr.Table.get("zawardo").?.Integer);
    var Num = try tokenstream.get(test_allocator, "HELL ad");
    try testing.expectEqual(@as(i64, 144), Num.Integer);
}

test "EXCUSE ME" {
    const test_string =
        \\hello = "123"
        \\world = { hello = 123, world = [123,], zawardo = 5}
        \\"HELL ad" = 144
        \\
    ;

    var tokenstream = TokenStream.init(test_string);
    _ = try tokenstream.next(); // =
    _ = try tokenstream.next(); // =
    _ = try tokenstream.next(); // =
    _ = try tokenstream.next(); // =
    _ = try tokenstream.next(); // =
    _ = try tokenstream.next(); // {
    _ = try tokenstream.next(); // =
    _ = try tokenstream.next(); // 123
    _ = try tokenstream.next(); // ,
    _ = try tokenstream.next(); // world
    _ = try tokenstream.next(); // =
    _ = try tokenstream.next(); //[
    _ = try tokenstream.next(); // 123
    _ = try tokenstream.next(); // ,
    _ = try tokenstream.next(); // ]
    _ = try tokenstream.next(); // ,
    _ = try tokenstream.next(); // =
    _ = try tokenstream.next(); // 5
    _ = try tokenstream.next(); // }
    try testing.expectEqual(Token.NewLine, tokenstream.token);
}

test "WHUTG" {
    const test_string =
        \\world = {hello = 345, wuat = [true,false]}
        \\
    ;
    var tokenstream = TokenStream.init(test_string);
    var arr = try tokenstream.get(test_allocator, "world");
    defer arr.deinit(test_allocator);
    std.debug.print("@#!{s}#!@\n", .{arr.Table.keys()});
    try testing.expectEqual(true, arr.Table.get("wuat").?.Array.items[0].Boolean);
}
test "WHUT" {
    const test_string =
        \\world = [{hello = 345, true = [{hell = true},false]}, 123]
        \\
    ;
    var tokenstream = TokenStream.init(test_string);
    var arr = try tokenstream.get(test_allocator, "world");
    defer arr.deinit(test_allocator);
    std.debug.print("@#!{s}#!@\n", .{arr.Array.items[0].Table.keys()});
    try testing.expectEqual(false, arr.Array.items[0].Table.get("true").?.Array.items[1].Boolean);
    try testing.expectEqual(true, arr.Array.items[0].Table.get("true").?.Array.items[0].Table.get("hell").?.Boolean);
    try testing.expectEqual(@as(i64, 123), arr.Array.items[1].Integer);
}
test "True As Key" {
    const test_string =
        \\waaat = false 
        \\
    ;
    var tokenstream = TokenStream.init(test_string);
    var arr = try tokenstream.get(test_allocator, "waaat");
    try testing.expectEqual(false, arr.Boolean);
}

test "NOT FOUND" {
    const test_string =
        \\waaat = false 
        \\
    ;
    var tokenstream = TokenStream.init(test_string);
    try testing.expectError(ParserError.NotFound, tokenstream.get(test_allocator, "XD"));
}
test "Quoted Key" {
    const test_string =
        \\"waaat" = false 
        \\
    ;
    var tokenstream = TokenStream.init(test_string);
    var arr = try tokenstream.get(test_allocator, "waaat");
    try testing.expectEqual(false, arr.Boolean);
}

test "EOF Bad Key" {
    const test_string =
        \\"waaat" = false 
        \\hell
        \\
    ;
    var tokenstream = TokenStream.init(test_string);
    try testing.expectError(ParserError.KeyWithNoValue, tokenstream.get(test_allocator, "hell"));
}

test "Table Parsing" {
    const test_string =
        \\ asdf = 123
        \\[newentry]
        \\[hello]
        \\"waaat" = false
        \\myname = "Idiot"
        \\yourname = "awesome"
        \\
    ;
    var tokenstream = TokenStream.init(test_string);
    var val = try tokenstream.get(test_allocator, "hello");
    defer val.deinit(test_allocator);
    var expected = [_][]const u8{ "waaat", "myname", "yourname" };
    for (expected, 0..) |should_be, idx| {
        try testing.expect(std.mem.eql(u8, should_be, val.Table.keys()[idx]));
    }
}

test "Entry inside table" {
    const test_string =
        \\hello = 1
        \\[newentry]
        \\[hello]
        \\"waaat" = false
        \\myname = "Idiot"
        \\yourname = "awesome"
        \\
    ;
    var tokenstream = TokenStream.init(test_string);
    var val = try tokenstream.get(test_allocator, "hello");
    defer val.deinit(test_allocator);
    try testing.expectError(ParserError.NotFound, tokenstream.get(test_allocator, "myname"));
}

test "Dotted" {
    const test_string =
        \\hell.o.ass = false 
        \\hello.o.ass.yo = false 
        \\
    ;
    var tokenstream = TokenStream.init(test_string);
    var arr = try tokenstream.get(test_allocator, "hello");
    defer arr.deinit(test_allocator);
    try testing.expectEqual(false, arr.Table.get("hello").?.Table.get("o").?.Table.get("ass").?
        .Table.get("yo").?.Boolean);
}

test "Dotted Table" {
    const test_string =
        \\[hell.o]
        \\abcd = false 
        \\
    ;
    var tokenstream = TokenStream.init(test_string);
    var arr = try tokenstream.get(test_allocator, "hell");
    defer arr.deinit(test_allocator);
    try testing.expectEqual(false, arr.Table.get("o").?.Table.get("abcd").?.Boolean);
}

test "Dotted Table 2" {
    const test_string =
        \\[hello]
        \\miss = 90
        \\[hell.o]
        \\abcd = false 
    ;
    var tokenstream = TokenStream.init(test_string);
    var arr = try tokenstream.get(test_allocator, "hell");
    defer arr.deinit(test_allocator);
    try testing.expectEqual(false, arr.Table.get("o").?.Table.get("abcd").?.Boolean);
}

test "From toml.io" {
    const test_string =
        \\title = "TOML Example"
        \\
        \\[owner]
        \\name = "Tom Preston-Werner"
        \\dob = 1979-05-27T07:32:00-08:00
        \\
        \\[database]
        \\enabled = true
        \\ports = [ 8000, 8001, 8002 ]
        \\data = [ ["delta", "phi"], [3.14] ]
        \\temp_targets = { cpu = 79.5, case = 72.0 }
        \\
        \\[servers]
        \\asdf = 0b111
        \\
        \\[servers.alpha]
        \\ip = "10.0.0.1"
        \\role = "frontend"
        \\
        \\[servers.beta]
        \\ip = "10.0.0.2"
        \\role = "backend"
    ;
    var tokenstream = TokenStream.init(test_string);
    var arr = try tokenstream.get(test_allocator, "servers");
    defer arr.deinit(test_allocator);
    var got = arr.Table.get("beta").?.Table.get("ip").?.String;
    var got1 = arr.Table.get("alpha").?.Table.get("ip").?.String;
    var seven = arr.Table.get("asdf").?.Integer;
    try testing.expect(std.mem.eql(u8, "10.0.0.2", got));
    try testing.expect(std.mem.eql(u8, "10.0.0.1", got1));
    try testing.expectEqual(@as(i64, 7), seven);
}

test "From toml.io with dotted inline" {
    const test_string =
        \\title = "TOML Example"
        \\
        \\[owner]
        \\name = "Tom Preston-Werner"
        \\dob = 1979-05-27T07:32:00-08:00
        \\
        \\[database]
        \\enabled = true
        \\ports = [ 8000, 8001, 8002 ]
        \\xxx.help = [ ["delta", "phi"], [3.14] ]
        \\temp_targets = { cpu.four = 79.5, case = 72.0 }
        \\
        \\[servers.alpha]
        \\ip.alpha = "10.0.0.1"
        \\role = "frontend"
        \\
        \\[servers.beta]
        \\ip = "10.0.0.2"
        \\role = "backend"
    ;
    var tokenstream = TokenStream.init(test_string);
    var arr = try tokenstream.get(test_allocator, "database");
    defer arr.deinit(test_allocator);
    var got = arr.Table.get("temp_targets").?.Table.get("cpu").?.Table.get("four").?.Float;
    try testing.expectEqual(@as(f64, 79.5), got);
}

test "deserialize a struct" {
    const random_struct = struct { random: []u8 };
    const idiot_struct = struct { idiot: random_struct };
    const inner = struct { test_field: bool, test_string: idiot_struct };
    const config = struct {
        test_struct: inner,
    };
    var toml =
        \\[test_struct]
        \\test_field = true
        \\#hello
        \\test_string = {idiot = {random = "hello world" } }
    ;
    var kk = try parse(config, test_allocator, toml);
    defer test_allocator.free(kk.test_struct.test_string.idiot.random);
    try testing.expectEqual(kk.test_struct.test_field, true);
    try testing.expect(std.mem.eql(u8, kk.test_struct.test_string.idiot.random, "hello world"));
}

test "deserialize a struct with an array of numbers" {
    const test_struct = struct { test_string: []i64, num: f64, another: []const u8 };
    const config = struct {
        test_struct: test_struct,
    };
    var toml =
        \\[test_struct]
        \\# This is a full-line comment
        \\key = "value"  # This is a comment at the end of a line
        \\another = "# This is not a comment"
        \\test_string = [1, #hello
        \\2,3]
        \\num = 3.4
    ;
    var kk = try parse(config, test_allocator, toml);
    defer test_allocator.free(kk.test_struct.another);
    defer test_allocator.free(kk.test_struct.test_string);
    try testing.expectEqual(@as(i64, 2), kk.test_struct.test_string[1]);
    try testing.expectEqual(@as(f64, 3.4), kk.test_struct.num);
    try testing.expect(std.mem.eql(u8, kk.test_struct.another, "# This is not a comment"));
}

test "basic serialization" {
    const test_struct = struct { num: i64, num2: f64, string: []const u8, array: [2]f64 };
    const config = struct {
        test_struct: test_struct,
    };
    var instance = test_struct{
        .num = 5,
        .num2 = 100.0,
        .string = "Hello, World",
        .array = .{ 33.3, 34.3 },
    };
    var ret = try serialize(test_struct, instance, test_allocator, false);
    defer ret.deinit();
    var back_to_struct = try parse(config, test_allocator, ret.items);
    defer test_allocator.free(back_to_struct.test_struct.string);
    //try testing.expect(back_to_struct.array.items.len == 0);
    try testing.expectEqual(@as(f64, 33.3), back_to_struct.test_struct.array[0]);
    try testing.expectEqual(@as(f64, 34.3), back_to_struct.test_struct.array[1]);
}
test "nested struct serialization" {
    const inner = struct { num: i64, num2: f64, string: []const u8, array: [10]f64 };
    const outer = struct { inner: inner };
    const config = struct {
        outer: outer,
    };
    var inner_instance = inner{
        .num = 5,
        .num2 = 100.0,
        .string = "Hello, World",
        .array = .{ 33.3, 34.3, 10.0, 420.69, 0, 0, 1, 8, 10, 9 },
    };
    var outer_instance = outer{
        .inner = inner_instance,
    };
    var ret = try serialize(outer, outer_instance, test_allocator, false);
    defer ret.deinit();
    std.debug.print("@@{s}@@\n", .{ret.items});
    var back_to_struct = try parse(config, test_allocator, ret.items);
    defer test_allocator.free(back_to_struct.outer.inner.string);
    try testing.expectEqual(@as(f64, 34.3), back_to_struct.outer.inner.array[1]);
}

test "test arena" {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    const test_struct = struct { num: i64, num2: f64, string: []const u8, array: [2]f64 };
    const config = struct {
        test_struct: test_struct,
    };
    var instance = test_struct{
        .num = 5,
        .num2 = 100.0,
        .string = "Hello, World",
        .array = .{ 33.3, 34.3 },
    };
    var ret = try serialize(test_struct, instance, allocator, false);
    var back_to_struct = try parse(config, allocator, ret.items);
    //try testing.expect(back_to_struct.array.items.len == 0);
    try testing.expectEqual(@as(f64, 33.3), back_to_struct.test_struct.array[0]);
    try testing.expectEqual(@as(f64, 34.3), back_to_struct.test_struct.array[1]);
}

test "test array table100" {
    var toml =
        \\hello = [1, 2, 3]
        \\[[servers]]
        \\ip = "10.0.0.1"
        \\role = "frontend"
        \\
        \\[[servers]]
        \\ip = "10.0.0.2"
        \\role = "backend"
        \\[tihi]
    ;
    var tokenstream = TokenStream.init(toml);
    var arr = try tokenstream.get(test_allocator, "servers");
    try testing.expect(std.mem.eql(u8, arr.Array.items[1].Table.get("ip").?.String, "10.0.0.2"));
    std.debug.print("!!!!{s}@!@!@!\n", .{arr.Array.items[1].Table.keys()});
    defer arr.deinit(test_allocator);
}

test "test array table 2" {
    var toml =
        \\hello = [1, 2, 3]
        \\[[servers]]
        \\
        \\[[servers]]
        \\ip = "10.0.0.1"
        \\role = "frontend"
        \\
        \\[[servers]]
        \\ip = "10.0.0.2"
        \\role = "backend"
    ;
    var tokenstream = TokenStream.init(toml);
    var arr = try tokenstream.get(test_allocator, "servers");
    defer arr.deinit(test_allocator);
    try testing.expectEqual(arr.Array.items.len, 3);
    try testing.expect(std.mem.eql(u8, arr.Array.items[2].Table.get("ip").?.String, "10.0.0.2"));
}

test "array table dotted" {
    var toml =
        \\hello = [1, 2 ,3]
        \\[[owner.1]]
        \\name = "tom preston-werner"
        \\dob = 1979-05-27t07:32:00-08:00
    ;
    var tokenstream = TokenStream.init(toml);
    var arr = try tokenstream.get(test_allocator, "owner");
    defer arr.deinit(test_allocator);
    try testing.expect(std.mem.eql(u8, arr.Array.items[0].Table.get("1").?.Table.get("name").?.String, "tom preston-werner"));
}

test "invalid array table" {
    var toml =
        \\[[owner]]
        \\name = "tom preston-werner"
        \\dob = 1979-05-27t07:32:00-08:00
    ;
    var tokenstream = TokenStream.init(toml);
    var arr = try tokenstream.get(test_allocator, "owner");
    defer arr.deinit(test_allocator);
}

test "sample from toml.io" {
    var toml =
        \\# This is a TOML document
        \\
        \\title = "TOML Example"
        \\
        \\[owner]
        \\name = "Tom Preston-Werner"
        \\dob = 1979-05-27T07:32:00-08:00
        \\
        \\[database]
        \\enabled = true
        \\ports = [ 8000, 8001, 8002 ]
        \\data = [ ["delta", "phi"], [3.14] ]
        \\temp_targets = { cpu = 79.5, case = 72.0 }
        \\
        \\[servers]
        \\
        \\[servers.alpha]
        \\ip = "10.0.0.1"
        \\role = "frontend"
        \\
        \\[servers.beta]
        \\ip = "10.0.0.2"
        \\role = "backend"
    ;
    var tokenstream = TokenStream.init(toml);
    var arr = try tokenstream.get(test_allocator, "servers");
    defer arr.deinit(test_allocator);
}

test "test array table" {
    var toml =
        \\[[owner]]
        \\name = "Tom Preston-Werner"
        \\dob = 1979-05-27T07:32:00-08:00
        \\
        \\[[owner.test]]
        \\hello = 123
        \\
        \\[[owner.test]]
        \\hello = 345
        \\
        \\[[owner]]
        \\hell = "ME"
        \\
        \\  [owner.test]
        \\  hello = 678
    ;
    var tokenstream = TokenStream.init(toml);
    var arr = try tokenstream.get(test_allocator, "owner");
    try testing.expectEqual(@as(i64, 678), arr.Array.items[1].Table.get("test").?.Table.get("hello").?.Integer);
    defer arr.deinit(test_allocator);
}

test "Bad Curly" {
    var toml =
        \\[[owner]]
        \\name = "Tom Preston-Werner"
        \\dob = 1979-05-27T07:32:00-08:00
        \\
        \\[[owner.test]]
        \\hello = { hello = [}] } 
        \\
        \\[[owner.test]]
        \\hello = 345
        \\
        \\[[owner]]
        \\hell = "ME"
        \\
        \\  [owner.test]
        \\  hello = 678
    ;
    var tokenstream = TokenStream.init(toml);
    try testing.expectError(ParserError.UnexpectedCurlyBracket, tokenstream.get(test_allocator, "owner"));
}

test "Bad Bracket" {
    var toml =
        \\[[owner]]
        \\name = "Tom Preston-Werner"
        \\dob = 1979-05-27T07:32:00-08:00
        \\
        \\[[owner.test]]
        \\hello = { hello = []] } 
        \\
        \\[[owner.test]]
        \\hello = 345
        \\
        \\[[owner]]
        \\hell = "ME"
        \\
        \\  [owner.test]
        \\  hello = 678
    ;
    var tokenstream = TokenStream.init(toml);
    try testing.expectError(ParserError.UnexpectedBracket, tokenstream.get(test_allocator, "owner"));
}

test "quote in key" {
    var toml =
        \\["owner".test]
        \\"na.me"."'test'" = "Tom Preston-Werner"
        \\dob = 1979-05-27T07:32:00-08:00
    ;
    var tokenstream = TokenStream.init(toml);
    var val = try tokenstream.get(test_allocator, "owner");
    defer val.deinit(test_allocator);
    std.debug.print("**{s}**\n", .{val.Table.keys()});
    std.debug.print("**{s}**\n", .{val.Table.get("test").?.Table.get("na.me").?.Table.keys()});
}

test "split test" {
    var hello = "\"He.llo\".World";
    var splitted = split(u8, hello);
    try testing.expect(std.mem.eql(u8, "\"He.llo\"", splitted.next().?));
    try testing.expect(std.mem.eql(u8, "World", splitted.next().?));
}

test "whitespace in table" {
    const xx =
        \\[ j . "ʞ" .'l' ]
        \\xxx = 123
    ;
    var tokenstream = TokenStream.init(xx);
    var val = try tokenstream.get(test_allocator, "j");
    defer val.deinit(test_allocator);
    try testing.expectEqual(@as(usize, 1), val.Table.keys().len);
    try testing.expect(std.mem.eql(u8, val.Table.keys()[0], "ʞ"));
    try testing.expect(std.mem.eql(u8, val.Table.get("ʞ").?.Table.keys()[0], "l"));
    try testing.expectEqual(@as(i64, 123), val.Table.get("ʞ").?.Table.get("l").?.Table.get("xxx").?.Integer);
}
